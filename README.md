# Databrew

A data flow brewer, logger & dashboard

## Installation

For the first time only:

```bash
npm install
```

### Configure the API server

```bash
npm run configure
```

### Launch the API server

```bash
npm run start
```

### Launch the daemon that handles pending actions

In another terminal:

```bash
npm run process-events
```

## API

To read Databrew API, open http://petstore.swagger.io/ and explore http://localhost:3000/swagger.json.

## Example

### Create a user

POST the following JSON to http://localhost:3000/users:

```json
{
  "name": "Mr X",
  "urlName": "mr-x",
  "password": "secret"
}
```

For example, using `curl`:

```bash
cat <<'EOF' | curl -X POST --header "Content-Type: application/json" --header "Accept: application/json" --data-binary @- "http://localhost:3000/users"
{
  "name": "Mr X",
  "urlName": "mr-x",
  "password": "secret"
}
EOF
```

### Login to retrieve user API key

POST the following JSON to http://localhost:3000/login:

```json
{
  "userName": "mr-x",
  "password": "secret"
}
```

For example, using `curl`:

```bash
cat <<'EOF' | curl -X POST --header "Content-Type: application/json" --header "Accept: application/json" --data-binary @- "http://localhost:3000/login"
{
  "userName": "mr-x",
  "password": "secret"
}
EOF
```

And retrieve the API key in field `data.apiKey` of the response.

### Create a dataset belonging to this user

POST the following JSON to http://localhost:3000/datasets:

```json
{
  "description": "A sample dataset",
  "name": "Test",
  "url": "http://localhost/",
  "urlName": "test",
  "urlNamespace": "mr-x"
}
```

For example, using `curl`:

```bash
curl -X POST --header "Content-Type: application/json" --header "Accept: application/json" -d "{
  \"description\": \"A sample dataset\",
  \"name\": \"Test\",
  \"url\": \"http://localhost/\",
  \"urlName\": \"test\",
  \"urlNamespace\": \"mr-x\"
}" "http://localhost:3000/datasets"
```

### Create actions linked to this dataset and belonging to this user

POST the following JSONs to http://localhost:3000/actions:

```json
{
  "dataset": {
    "urlName": "test",
    "urlNamespace": "mr-x"
  },
  "kind": "ShellScriptAction",
  "name": "Convert french tax and benefit tables from XLSX to YAML raw",
  "script": [
    "cd `mktemp -d --suffix=databrew`",
    "git clone git@git.framasoft.org:french-tax-and-benefit-tables/ipp-tax-and-benefit-tables-converters.git",
    "cd ipp-tax-and-benefit-tables-converters",
    "git clone git@git.framasoft.org:french-tax-and-benefit-tables/ipp-tax-and-benefit-tables-xlsx.git xlsx",
    "git clone git@git.framasoft.org:french-tax-and-benefit-tables/ipp-tax-and-benefit-tables-yaml-raw.git yaml-raw",
    "./ipp_tax_and_benefit_tables_xlsx_to_xls.py",
    "./ipp_tax_and_benefit_tables_xls_to_yaml.py",
    "cd yaml-raw",
    "git add .",
    "git commit -m'Mise à jour à partir des fichiers XLSX'",
    "git push"
  ],
  "urlName": "french-tax-and-benefit-tables-xslx-to-yaml-raw",
  "urlNamespace": "mr-x"
}
```

```json
{
  "dataset": {
    "urlName": "test",
    "urlNamespace": "mr-x"
  },
  "kind": "SecureShellScriptAction",
  "host": "scripts.databrew.org",
  "username": "databrew",
  "name": "Remote Test",
  "script": "#!/bin/bash\nuptime\necho 'It works!'\necho 'last line'",
  "urlName": "ssh-test",
  "urlNamespace": "mr-x"
}
```

### Queue an action

POST to http://localhost:3000/actions/mr-x/ssh-test/queue.

For example, using `curl`:

```bash
curl -X POST --header "Accept: application/json" --header "Databrew-API-Key: 20RM2tFPxeD10Bur9ORuLw" "http://localhost:3000/actions/mr-x/ssh-test/queue"
```
