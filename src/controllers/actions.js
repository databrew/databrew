// Databrew -- A data flow brewer, logger & dashboard
// By: Emmanuel Raviart <emmanuel@raviart.com>
//
// Copyright (C) 2015 Emmanuel Raviart
// https://git.framasoft.org/databrew/databrew
//
// Databrew is free software; you can redistribute it and/or modify
// it under the terms of the GNU Affero General Public License as
// published by the Free Software Foundation, either version 3 of the
// License, or (at your option) any later version.
//
// Databrew is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Affero General Public License for more details.
//
// You should have received a copy of the GNU Affero General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.


import assert from "assert"
import {exec} from "mz/child_process"
import coBody from "co-body"
import {randomBytes} from "mz/crypto"
import fs from "mz/fs"
import os from "os"
import path from "path"
import getRawBody from "raw-body"

import {r} from "../database"
import {ownsNamespace} from "../model"


export function checkKey(require) {
  return async function checkKey(ctx, next) {
    let action = ctx.action
    let key = ctx.parameter["databrew-action-key"]
    if (!key) {
      if (require) {
        ctx.status = 401  // Unauthorized
        ctx.body = {
          apiVersion: "1",
          code: 401,  // Unauthorized
          message: "Databrew-Action-Key header is required.",
        }
        return
      }
    } else if (key != action.key) {
      ctx.status = 403  // Forbidden
      ctx.body = {
        apiVersion: "1",
        code: 403,  // Forbidden
        message: "Given key doesn't match the one of action.",
      }
      return
    } else {
      ctx.actionKey = key
    }
    await next()
  }
}


export {create}
async function create(ctx) {
  // Create a new action.
  let action = ctx.parameter.action

  action.createdAt = r.now()
  delete action.id
  action.key = (await randomBytes(16)).toString("hex")  // 128 bits
  if (!action.name) action.name = action.urlName
  let urlNamespace = action.urlNamespace
  delete action.urlNamespace

  let namespaces = await r
    .table("namespaces")
    .getAll(urlNamespace, {index: "urlName"})
    .limit(1)
  if (namespaces.length < 1) {
    ctx.status = 422  // Unprocessable Entity
    ctx.body = {
      apiVersion: "1",
      code: 422,  // Unprocessable Entity
      message: `No namespace with urlName "${urlNamespace}".`,
    }
    return
  }
  let namespace = namespaces[0]

  if (!(await ownsNamespace(ctx, ctx.authenticatedUser, namespace))) {
    ctx.status = 403  // Forbidden
    ctx.body = {
      apiVersion: "1",
      code: 403,  // Forbidden
      message: "Authenticated user can't create action in this namespace.",
    }
    return
  }

  action.namespaceId = namespace.id

  if (action.kind == "SecureShellScriptAction") {
    let actionUrl = `${ctx.protocol}://${ctx.host}/actions/${urlNamespace}/${action.urlName}`
    let token = (await randomBytes(16)).toString("hex")  // 128 bits token.
    let privateKeyPath = path.join(os.tmpDir(), `databrew-ssh-key-${token}`)
    await exec(`ssh-keygen -C "${actionUrl}" -f ${privateKeyPath} -N "" -q`)
    action.privateKey = (await fs.readFile(privateKeyPath)).toString()
    await fs.unlink(privateKeyPath)
    let publicKeyPath = privateKeyPath + ".pub"
    action.publicKey = (await fs.readFile(publicKeyPath)).toString().trim()
    await fs.unlink(publicKeyPath)
  }

  let result = await r
    .table("actions")
    .insert(action, {returnChanges: true})
  action = result.changes[0].new_val
  ctx.status = 201  // Created
  ctx.body = {
    apiVersion: "1",
    data: toActionJson(action, namespace.urlName, {showKey: true, showPrivate: true}),
  }
}


export {del}
async function del(ctx) {
  // Delete an existing action.
  let action = ctx.action

  if (!(await ownsNamespace(ctx, ctx.authenticatedUser, ctx.actionNamespace))) {
    ctx.status = 403  // Forbidden
    ctx.body = {
      apiVersion: "1",
      code: 403,  // Forbidden
      message: "Authenticated user doesn't own action.",
    }
    return
  }

  // Delete jobs of action.
  await r
    .table("jobs")
    .getAll(action.id, {index: "actionId"})
    .delete()

  // Delete action.
  await r
    .table("actions")
    .get(action.id)
    .delete()
  ctx.body = {
    apiVersion: "1",
    data: toActionJson(action, ctx.actionNamespace.urlName),
  }
}


export {deleteJobs}
async function deleteJobs(ctx) {
  // Delete all jobs of an action.
  let action = ctx.action

  if (!(await ownsNamespace(ctx, ctx.authenticatedUser, ctx.actionNamespace))) {
    ctx.status = 403  // Forbidden
    ctx.body = {
      apiVersion: "1",
      code: 403,  // Forbidden
      message: "Authenticated user doesn't own action.",
    }
    return
  }

  await r
    .table("jobs")
    .getAll(action.id, {index: "actionId"})
    .delete()
  ctx.body = {
    apiVersion: "1",
    message: `Jobs of action "${ctx.actionNamespace.urlName}/${action.urlName}" have been deleted.`,
  }
}


export {get}
async function get(ctx) {
  // Respond an existing action.
  let show = ctx.parameter.show || []
  let showKey = show.includes("key")
  let showPrivate = show.includes("private")

  if ((showKey || showPrivate) && !(await ownsNamespace(ctx, ctx.authenticatedUser, ctx.actionNamespace))) {
    ctx.status = 403  // Forbidden
    ctx.body = {
      apiVersion: "1",
      code: 403,  // Forbidden
      message: showKey ?
        "Authenticated user can't view key of action." :
        "Authenticated user can't view private data of action.",
    }
    return
  }

  ctx.body = {
    apiVersion: "1",
    data: toActionJson(ctx.action, ctx.actionNamespace.urlName, {showKey, showPrivate}),
  }
}


export {getScript}
async function getScript(ctx) {
  // Respond the script of an existing action.
  ctx.type = "text/plain; charset=utf-8"
  ctx.body = ctx.action.script
}


export {list}
async function list(ctx) {
  // Respond a list of all actions.
  let items = await r
    .table("actions")
    .orderBy({index: r.desc("createdAt")})
    .eqJoin("namespaceId", r.table("namespaces"))
    .pluck({left: "urlName", right: "urlName"})
  ctx.body = {
    apiVersion: "1",
    data: items.map(item => ({
      urlName: item.left.urlName,
      urlNamespace: item.right.urlName,
    })),
  }
}


export {listJobs}
async function listJobs(ctx) {
  // Respond a list of all jobs of an action.
  let action = ctx.action

  let jobs = await r
    .table("jobs")
    .getAll(action.id, {index: "actionId"})
    .orderBy(r.desc("createdAt"))
  ctx.body = {
    apiVersion: "1",
    data: jobs.map(job => ({
      createdAt: job.createdAt.toISOString(),
      urlName: action.urlName,
      urlNamespace: ctx.actionNamespace.urlName,
    })),
  }
}


export {listNamespace}
async function listNamespace(ctx) {
  // Respond a list of all actions in given namespace.
  let namespace = ctx.actionNamespace
  let urlNames = await r
    .table("actions")
    .getAll(namespace.id, {index: "namespaceId"})
    .getField("urlName")
  ctx.body = {
    apiVersion: "1",
    data: urlNames.map(urlName => ({
      urlName: urlName,
      urlNamespace: namespace.urlName,
    })),
  }
}


export function listSourcesOrTargets(fieldName) {
  assert(["sourcesId", "targetsId"].includes(fieldName))
  return async function listSourcesOrTargets(ctx) {
    // Respond a list of event types consumed or generated by action.
    let action = ctx.action

    let eventTypesId = action[fieldName]
    let eventTypesReference = []
    if (eventTypesId) {
      let eventTypes = await r
        .table("eventTypes")
        .getAll(...eventTypesId)
      for (let eventType of eventTypes) {
        let dataset = await r
          .table("datasets")
          .get(eventType.datasetId)
        if (!dataset) {
          console.log(`Skipping missing target dataset ${eventType.datasetId} for event "${eventType.urlNane}".`)
          continue
        }
        let datasetNamespace = await r
          .table("namespaces")
          .get(dataset.namespaceId)
        if (!datasetNamespace) {
          console.log(`Skipping target dataset ${eventType.datasetId} (urlName ${dataset.urlName}) without namespace.`)
          continue
        }
        eventTypesReference.push({
          dataset: {
            urlName: dataset.urlName,
            urlNamespace: datasetNamespace.urlName,
          },
          urlName: eventType.urlName,
        })
      }
    }
    ctx.body = {
      apiVersion: "1",
      data: eventTypesReference,
    }
  }
}


export {notifyTargets}
async function notifyTargets(ctx) {
  // Notify an action (ie its targets) of an event, originating from a job (a script).
  let action = ctx.action
  let authenticatedUser = ctx.authenticatedUser
  let body = (await getRawBody(ctx.req, {
    length: ctx.length,
    limit: "1mb",
    encoding: ctx.charset,
  })).toString()
  let job = ctx.job

  if (!job && !authenticatedUser) {
    ctx.status = 401  // Unauthorized
    ctx.body = {
      apiVersion: "1",
      code: 401,  // Unauthorized
      message: "Either an authentication or Databrew-Job-Key header is required.",
    }
    return
  }
  if (job && job.actionId != action.id) {
    ctx.status = 403  // Forbidden
    ctx.body = {
      apiVersion: "1",
      code: 403,  // Forbidden
      message: "Job matching given key doesn't belong to given action.",
    }
    return
  }
  if (!job && !(await ownsNamespace(ctx, authenticatedUser, ctx.actionNamespace))) {
    ctx.status = 403  // Forbidden
    ctx.body = {
      apiVersion: "1",
      code: 403,  // Forbidden
      message: "Authenticated user doesn't own given action.",
    }
    return
  }

  let targetsPath = []
  if (action.targetsId) {
    let eventTypes = await r
      .table("eventTypes")
      .getAll(...action.targetsId)
    for (let eventType of eventTypes) {
      let dataset = await r
        .table("datasets")
        .get(eventType.datasetId)
      if (!dataset) {
        console.log(`Skipping missing target dataset ${eventType.datasetId} for event "${eventType.urlNane}".`)
        continue
      }
      let datasetNamespace = await r
        .table("namespaces")
        .get(dataset.namespaceId)
      if (!datasetNamespace) {
        console.log(`Skipping target dataset ${eventType.datasetId} (urlName ${dataset.urlName}) without namespace.`)
        continue
      }
      let event = {
        body: body,
        createdAt: r.now(),
        headers: ctx.headers,
        method: ctx.method,
        querystring: ctx.querystring,
        remoteAddress: ctx.req.connection.remoteAddress,
        remotePort: ctx.req.connection.remotePort,
        typeId: eventType.id,
      }
      let result = await r
        .table("events")
        .insert(event, {returnChanges: true})
      event = result.changes[0].new_val
      targetsPath.push(`${datasetNamespace.urlName}/${dataset.urlName}/${eventType.urlName}`)
    }
  }
  ctx.body = {
    apiVersion: "1",
    message: targetsPath.length == 0 ? `No event has been sent.` :
      targetsPath.length == 1 ? `An event has been sent to ${targetsPath.join(", ")}.` :
      `Events have been sent to ${targetsPath.join(", ")}.`,
  }
}


export {patch}
async function patch(ctx) {
  // Update some attributes of an existing action.
  let action = ctx.action
  let patch = ctx.parameter.patch

  if (!(await ownsNamespace(ctx, ctx.authenticatedUser, ctx.actionNamespace))) {
    ctx.status = 403  // Forbidden
    ctx.body = {
      apiVersion: "1",
      code: 403,  // Forbidden
      message: "Authenticated user doesn't own action.",
    }
    return
  }

  let patched = false
  for (let attributeName in patch) {
    if (["description", "name"].includes(attributeName)) {
      let attributeValue = patch[attributeName]
      if (attributeValue === null) {
        if (action[attributeName] !== undefined) {
          delete action[attributeName]
          patched = true
        }
      } else if (attributeValue != action[attributeName]) {
        action[attributeName] = attributeValue
        patched = true
      }
    } else if (["mergePending"].includes(attributeName)) {
      let attributeValue = patch[attributeName]
      if (!attributeValue) {
        if (action[attributeName] !== undefined) {
          delete action[attributeName]
          patched = true
        }
      } else if (action[attributeName] !== true) {
        action[attributeName] = true
        patched = true
      }
    } else if (["private"].includes(attributeName)) {
      let attributeValue = patch[attributeName]
      if (attributeValue === null) {
        if (action[attributeName] !== undefined) {
          delete action[attributeName]
          patched = true
        }
      } else {
        let existingAttributeValue = action[attributeName]
        if (!existingAttributeValue) action[attributeName] = existingAttributeValue = {}
        for (let subAttributeName in attributeValue) {
          let subAttributeValue = attributeValue[subAttributeName]
          if (subAttributeValue === null) {
            if (existingAttributeValue[subAttributeName] !== undefined) {
              delete existingAttributeValue[subAttributeName]
              patched = true
            }
          } else if (subAttributeValue != existingAttributeValue[subAttributeName]) {
            existingAttributeValue[subAttributeName] = subAttributeValue
            patched = true
          }
        }
        if (Object.getOwnPropertyNames(existingAttributeValue).length === 0) delete action[attributeName]
      }
    }
  }
  if (patched) {
    let result = await r
      .table("actions")
      .get(action.id)
      .replace(action, {returnChanges: true})
    action = result.changes[0].new_val
  }
  ctx.body = {
    apiVersion: "1",
    data: toActionJson(action, ctx.actionNamespace.urlName, {showKey: true, showPrivate: true}),
  }
}


export {registerAndNotifySource}
async function registerAndNotifySource(ctx) {
  // Register dataset to list of datasets used by action and notify it of a "get" event.
  let action = ctx.action
  let authenticatedUser = ctx.authenticatedUser
  let job = ctx.job

  if (!job && !authenticatedUser) {
    ctx.status = 401  // Unauthorized
    ctx.body = {
      apiVersion: "1",
      code: 401,  // Unauthorized
      message: "Either an authentication or Databrew-Job-Key header is required.",
    }
    return
  }
  if (job && job.actionId != action.id) {
    ctx.status = 403  // Forbidden
    ctx.body = {
      apiVersion: "1",
      code: 403,  // Forbidden
      message: "Job matching given key doesn't belong to given action.",
    }
    return
  }
  if (!job && !(await ownsNamespace(ctx, authenticatedUser, ctx.actionNamespace))) {
    ctx.status = 403  // Forbidden
    ctx.body = {
      apiVersion: "1",
      code: 403,  // Forbidden
      message: "Authenticated user doesn't own given action.",
    }
    return
  }

  let datasetNamespace = ctx.datasetNamespace
  let datasetUrlName = ctx.parameter.datasetName
  let datasets = await r
    .table("datasets")
    .getAll([datasetNamespace.id, datasetUrlName], {index: "namespaceIdAndUrlName"})
    .limit(1)
  let dataset
  if (datasets.length < 1) {
    // Create dataset.

    // TOOO:
    // if dataset doesn't exist:
    //   if authenticatedUser is given:
    //     check that authenticatedUser can create the dataset.
    //   else  // Job has been authenticated by its key.
    //     check that action's owner can create the dataset.

    dataset = {
      createdAt: r.now(),
      eventKey: (await randomBytes(16)).toString("hex"),  // 128 bits
      name: datasetUrlName,  // TODO: Use clean ctx.parameter.datasetName instead of urlName.
      namespaceId: datasetNamespace.id,
      urlName: datasetUrlName,
    }
    let result = await r
      .table("datasets")
      .insert(dataset, {returnChanges: true})
    dataset = result.changes[0].new_val
  } else {
    dataset = datasets[0]
  }

  let eventTypeUrlName = ctx.parameter.eventTypeName
  let eventTypes = await r
    .table("eventTypes")
    .getAll([dataset.id, eventTypeUrlName], {index: "datasetIdAndUrlName"})
    .limit(1)
  let eventType
  if (eventTypes.length < 1) {
    // Create event type.
    eventType = {
      createdAt: r.now(),
      datasetId: dataset.id,
      name: eventTypeUrlName,  // TODO: Use clean ctx.parameter.eventTypeName instead of urlName.
      urlName: eventTypeUrlName,
    }
    let result = await r
      .table("eventTypes")
      .insert(eventType, {returnChanges: true})
    eventType = result.changes[0].new_val
  } else {
    eventType = eventTypes[0]
  }

  let eventTypesId = action.sourcesId || []
  if (!eventTypesId.includes(eventType.id)) {
    eventTypesId.push(eventType.id)
    let actionUpdate = {
      sourcesId: eventTypesId,
    }
    await r
      .table("actions")
      .get(action.id)
      .update(actionUpdate)
  }

  ctx.body = {
    apiVersion: "1",
    data: await toEventTypesReferenceJson(r, eventTypesId),
  }
}


export {registerAndNotifyTarget}
async function registerAndNotifyTarget(ctx) {
  // Register dataset to list of datasets generated by action and notify it of an "update" event.
  let action = ctx.action
  let authenticatedUser = ctx.authenticatedUser
  let body = (await getRawBody(ctx.req, {
    length: ctx.length,
    limit: "1mb",
    encoding: ctx.charset,
  })).toString()
  let job = ctx.job

  if (!job && !authenticatedUser) {
    ctx.status = 401  // Unauthorized
    ctx.body = {
      apiVersion: "1",
      code: 401,  // Unauthorized
      message: "Either an authentication or Databrew-Job-Key header is required.",
    }
    return
  }
  if (job && job.actionId != action.id) {
    ctx.status = 403  // Forbidden
    ctx.body = {
      apiVersion: "1",
      code: 403,  // Forbidden
      message: "Job matching given key doesn't belong to given action.",
    }
    return
  }
  if (!job && !(await ownsNamespace(ctx, authenticatedUser, ctx.actionNamespace))) {
    ctx.status = 403  // Forbidden
    ctx.body = {
      apiVersion: "1",
      code: 403,  // Forbidden
      message: "Authenticated user doesn't own given action.",
    }
    return
  }

  // TOOO:
  //   if authenticatedUser is given:
  //     check that authenticatedUser owns the dataset or can create it.
  //   else  // Job has been authenticated by its key.
  //     check that action's owner owns the dataset or can create it.

  let datasetNamespace = ctx.datasetNamespace
  let datasetUrlName = ctx.parameter.datasetName
  let datasets = await r
    .table("datasets")
    .getAll([datasetNamespace.id, datasetUrlName], {index: "namespaceIdAndUrlName"})
    .limit(1)
  let dataset
  if (datasets.length < 1) {
    // Create dataset.
    dataset = {
      createdAt: r.now(),
      eventKey: (await randomBytes(16)).toString("hex"),  // 128 bits
      name: datasetUrlName,  // TODO: Use clean ctx.parameter.datasetName instead of urlName.
      namespaceId: datasetNamespace.id,
      urlName: datasetUrlName,
    }
    let result = await r
      .table("datasets")
      .insert(dataset, {returnChanges: true})
    dataset = result.changes[0].new_val
  } else {
    dataset = datasets[0]
  }

  let eventTypeUrlName = ctx.parameter.eventTypeName
  let eventTypes = await r
    .table("eventTypes")
    .getAll([dataset.id, eventTypeUrlName], {index: "datasetIdAndUrlName"})
    .limit(1)
  let eventType
  if (eventTypes.length < 1) {
    // Create event type.
    eventType = {
      createdAt: r.now(),
      datasetId: dataset.id,
      name: eventTypeUrlName,  // TODO: Use clean ctx.parameter.eventTypeName instead of urlName.
      urlName: eventTypeUrlName,
    }
    let result = await r
      .table("eventTypes")
      .insert(eventType, {returnChanges: true})
    eventType = result.changes[0].new_val
  } else {
    eventType = eventTypes[0]
  }

  let eventTypesId = action.targetsId || []
  if (!eventTypesId.includes(eventType.id)) {
    eventTypesId.push(eventType.id)
    let actionUpdate = {
      targetsId: eventTypesId,
    }
    await r
      .table("actions")
      .get(action.id)
      .update(actionUpdate)
  }

  let event = {
    body: body,
    createdAt: r.now(),
    headers: ctx.headers,
    method: ctx.method,
    querystring: ctx.querystring,
    remoteAddress: ctx.req.connection.remoteAddress,
    remotePort: ctx.req.connection.remotePort,
    typeId: eventType.id,
  }
  let result = await r
    .table("events")
    .insert(event, {returnChanges: true})
  event = result.changes[0].new_val

  ctx.body = {
    apiVersion: "1",
    data: await toEventTypesReferenceJson(r, eventTypesId),
  }
}


export function removeSourceOrTarget(fieldName) {
  assert(["sourcesId", "targetsId"].includes(fieldName))
  return async function removeSourceOrTarget(ctx) {
    // Remove an event type from the list of event types generated or received by action.
    let action = ctx.action
    let authenticatedUser = ctx.authenticatedUser
    let eventType = ctx.eventType
    let job = ctx.job

    if (!job && !authenticatedUser) {
      ctx.status = 401  // Unauthorized
      ctx.body = {
        apiVersion: "1",
        code: 401,  // Unauthorized
        message: "Either an authentication or Databrew-Job-Key header is required.",
      }
      return
    }
    if (job && job.actionId != action.id) {
      ctx.status = 403  // Forbidden
      ctx.body = {
        apiVersion: "1",
        code: 403,  // Forbidden
        message: "Job matching given key doesn't belong to given action.",
      }
      return
    }
  if (!job && !(await ownsNamespace(ctx, authenticatedUser, ctx.actionNamespace))) {
    ctx.status = 403  // Forbidden
    ctx.body = {
      apiVersion: "1",
      code: 403,  // Forbidden
      message: "Authenticated user doesn't own given action.",
    }
    return
  }

    let eventTypesId = action[fieldName] || []
    let eventTypeIndex = eventTypesId.indexOf(eventType.id)
    if (eventTypeIndex > -1) {
      eventTypesId.splice(eventTypeIndex, 1)
      let actionUpdate = {}
      actionUpdate[fieldName] = eventTypesId
      await r
        .table("actions")
        .get(action.id)
        .update(actionUpdate)
    }

    ctx.body = {
      apiVersion: "1",
      data: await toEventTypesReferenceJson(r, eventTypesId),
    }
  }
}


export {requireAction}
async function requireAction(ctx, next) {
  let namespace = ctx.actionNamespace
  let urlName
  if (ctx.parameter) {
    urlName = ctx.parameter.actionName  // koa-swager
  } else {
    // koa-validate
    ctx.checkParams("actionName")
      .notEmpty()
    urlName = ctx.params.actionName
  }
  let actions = await r
    .table("actions")
    .getAll([namespace.id, urlName], {index: "namespaceIdAndUrlName"})
    .limit(1)
  if (actions.length < 1) {
    ctx.status = 404
    ctx.body = {
      apiVersion: "1",
      code: 404,
      message: `No action named "${urlName}" in namespace "${namespace.urlName}".`,
    }
    return
  }
  ctx.action = actions[0]

  await next()
}


export {setScript}
async function setScript(ctx) {
  // Set the script of an existing action.
  let action = ctx.action
  let script = await coBody.text(ctx.request)

  if (!(await ownsNamespace(ctx, ctx.authenticatedUser, ctx.actionNamespace))) {
    ctx.status = 403  // Forbidden
    ctx.body = {
      apiVersion: "1",
      code: 403,  // Forbidden
      message: "Authenticated user doesn't own action.",
    }
    return
  }

  await r
    .table("actions")
    .get(action.id)
    .update({script: script})
  action = await r
    .table("actions")
    .get(action.id)
  ctx.body = {
    apiVersion: "1",
    data: toActionJson(action, ctx.actionNamespace.urlName),
  }
}


function toActionJson(action, urlNamespace, {showKey = true, showPrivate = false} = {}) {
  let actionJson = {...action}
  actionJson.createdAt = actionJson.createdAt.toISOString()
  delete actionJson.id
  if (!showKey) delete actionJson.key
  delete actionJson.namespaceId
  if (!showPrivate) delete actionJson.private
  delete actionJson.privateKey
  delete actionJson.sourcesId  // TODO: Return a list of references of datasets.
  delete actionJson.targetsId  // TODO: Return a list of references of datasets.
  actionJson.urlNamespace = urlNamespace
  return actionJson
}


async function toEventTypesReferenceJson(r, eventTypesId) {
  if (eventTypesId.length === 0) return []
  let eventTypesReference = []
  for (let eventTypeId of eventTypesId) {
    let eventType = await r
      .table("eventTypes")
      .get(eventTypeId)
    if (!eventType) continue
    let dataset = await r
      .table("datasets")
      .get(eventType.datasetId)
    if (!dataset) continue
    let namespace = await r
      .table("namespaces")
      .get(dataset.namespaceId)
    if (!namespace) continue
    eventTypesReference.push({
      dataset: {
        urlName: dataset.urlName,
        urlNamespace: namespace.urlName,
      },
      urlName: eventType.urlName,
    })
  }
  return eventTypesReference
}
