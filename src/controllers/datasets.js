// Databrew -- A data flow brewer, logger & dashboard
// By: Emmanuel Raviart <emmanuel@raviart.com>
//
// Copyright (C) 2015 Emmanuel Raviart
// https://git.framasoft.org/databrew/databrew
//
// Databrew is free software; you can redistribute it and/or modify
// it under the terms of the GNU Affero General Public License as
// published by the Free Software Foundation, either version 3 of the
// License, or (at your option) any later version.
//
// Databrew is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Affero General Public License for more details.
//
// You should have received a copy of the GNU Affero General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.


import {randomBytes} from "mz/crypto"

import {r} from "../database"
import {ownsNamespace} from "../model"


export function checkEventKey(require) {
  return async function checkEventKey(ctx, next) {
    let dataset = ctx.dataset
    let eventKey = ctx.parameter["databrew-event-key"] ||  ctx.parameter.eventKey
    if (!eventKey) {
      if (require) {
        ctx.status = 401  // Unauthorized
        ctx.body = {
          apiVersion: "1",
          code: 401,  // Unauthorized
          message: "Databrew-Event-Key header or eventKey parameter is required.",
        }
        return
      }
    } else if (eventKey != dataset.eventKey) {
      ctx.status = 403  // Forbidden
      ctx.body = {
        apiVersion: "1",
        code: 403,  // Forbidden
        message: "Given event key doesn't match the one of dataset.",
      }
      return
    } else {
      ctx.eventKey = eventKey
    }
    await next()
  }
}


export {create}
async function create(ctx) {
  // Create a new dataset.
  let dataset = ctx.parameter.dataset

  dataset.createdAt = r.now()
  dataset.eventKey = (await randomBytes(16)).toString("hex")  // 128 bits
  delete dataset.id
  if (!dataset.name) dataset.name = dataset.urlName
  let urlNamespace = dataset.urlNamespace
  delete dataset.urlNamespace
  let namespaces = await r
    .table("namespaces")
    .getAll(urlNamespace, {index: "urlName"})
    .limit(1)
  if (namespaces.length < 1) {
    ctx.status = 422  // Unprocessable Entity
    ctx.body = {
      apiVersion: "1",
      code: 422,  // Unprocessable Entity
      message: `No namespace with urlName "${urlNamespace}".`,
    }
    return
  }
  let namespace = namespaces[0]

  if (!(await ownsNamespace(ctx, ctx.authenticatedUser, namespace))) {
    ctx.status = 403  // Forbidden
    ctx.body = {
      apiVersion: "1",
      code: 403,  // Forbidden
      message: "Authenticated user can't create dataset in this namespace.",
    }
    return
  }

  dataset.namespaceId = namespace.id
  let result = await r
    .table("datasets")
    .insert(dataset, {returnChanges: true})
  dataset = result.changes[0].new_val
  ctx.status = 201  // Created
  ctx.body = {
    apiVersion: "1",
    data: toDatasetJson(dataset, urlNamespace, {showEventKey: true}),
  }
}


export {del}
async function del(ctx) {
  // Delete an existing dataset.
  let dataset = ctx.dataset

  if (!(await ownsNamespace(ctx, ctx.authenticatedUser, ctx.datasetNamespace))) {
    ctx.status = 403  // Forbidden
    ctx.body = {
      apiVersion: "1",
      code: 403,  // Forbidden
      message: "Authenticated user doesn't own dataset.",
    }
    return
  }

  // Delete event types & events of dataset.
  let eventTypes = await r
    .table("eventTypes")
    .getAll(dataset.id, {index: "datasetId"})
  for (let eventType of eventTypes) {
    await r
      .table("events")
      .getAll(eventType.id, {index: "typeId"})
      .delete()
  }
  await r
    .table("eventTypes")
    .getAll(dataset.id, {index: "datasetId"})
    .delete()

  // Delete dataset.
  await r
    .table("datasets")
    .get(dataset.id)
    .delete()
  ctx.body = {
    apiVersion: "1",
    data: toDatasetJson(dataset, ctx.datasetNamespace.urlName),
  }
}


export {get}
async function get(ctx) {
  // Respond an existing dataset.
  let show = ctx.parameter.show || []
  let showEventKey = show.includes("eventKey")

  if (showEventKey && !(await ownsNamespace(ctx, ctx.authenticatedUser, ctx.datasetNamespace))) {
    ctx.status = 403  // Forbidden
    ctx.body = {
      apiVersion: "1",
      code: 403,  // Forbidden
      message: "Authenticated user can't view the event key of dataset.",
    }
    return
  }

  ctx.body = {
    apiVersion: "1",
    data: toDatasetJson(ctx.dataset, ctx.datasetNamespace.urlName, {showEventKey}),
  }
}


export {list}
async function list(ctx) {
  // Respond a list of all datasets.
  let items = await r
    .table("datasets")
    .orderBy({index: r.desc("createdAt")})
    .eqJoin("namespaceId", r.table("namespaces"))
    .pluck({left: "urlName", right: "urlName"})
  ctx.body = {
    apiVersion: "1",
    data: items.map(item => ({
      urlName: item.left.urlName,
      urlNamespace: item.right.urlName,
    })),
  }
}


export {listActions}
async function listActions(ctx) {
  // Respond a list of all actions triggered by events occuring to a dataset.
  let dataset = ctx.dataset

  let actionsReference = await r
    .table("actions")
    .getAll(dataset.id, {index: "sourcesId"})
    .distinct()
    .eqJoin("namespaceId", r.table("namespaces"))
    .orderBy(r.desc("createdAt"))
    .map(function (item) {
      return {
        urlName: item("left")("urlName"),
        urlNamespace: item("right")("urlName"),
      }
    })
  ctx.body = {
    apiVersion: "1",
    data: actionsReference,
  }
}


export {listNamespace}
async function listNamespace(ctx) {
  // Respond a list of all datasets.
  let namespace = ctx.datasetNamespace
  let urlNames = await r
    .table("datasets")
    .getAll(namespace.id, {index: "namespaceId"})
    .getField("urlName")
  ctx.body = {
    apiVersion: "1",
    data: urlNames.map(urlName => ({
      urlName: urlName,
      urlNamespace: namespace.urlName,
    })),
  }
}


export {patch}
async function patch(ctx) {
  // Update some attributes of an existing dataset.
  let dataset = ctx.dataset
  let patch = ctx.parameter.patch

  if (!(await ownsNamespace(ctx, ctx.authenticatedUser, ctx.datasetNamespace))) {
    ctx.status = 403  // Forbidden
    ctx.body = {
      apiVersion: "1",
      code: 403,  // Forbidden
      message: "Authenticated user doesn't own dataset.",
    }
    return
  }

  let patched = false
  for (let attributeName in patch) {
    if (["description", "name", "url"].includes(attributeName)) {
      let attributeValue = patch[attributeName]
      if (attributeValue === null) {
        if (dataset[attributeName] !== undefined) {
          delete dataset[attributeName]
          patched = true
        }
      } else if (attributeValue != dataset[attributeName]) {
        dataset[attributeName] = attributeValue
        patched = true
      }
    }
  }
  if (patched) {
    let result = await r
      .table("datasets")
      .get(dataset.id)
      .replace(dataset, {returnChanges: true})
    dataset = result.changes[0].new_val
  }
  ctx.body = {
    apiVersion: "1",
    data: toDatasetJson(dataset, ctx.datasetNamespace.urlName, {showEventKey: true}),
  }
}


export {requireDataset}
async function requireDataset(ctx, next) {
  let namespace = ctx.datasetNamespace
  let urlName = ctx.parameter.datasetName
  let datasets = await r
    .table("datasets")
    .getAll([namespace.id, urlName], {index: "namespaceIdAndUrlName"})
    .limit(1)
  if (datasets.length < 1) {
    ctx.status = 404
    ctx.body = {
      apiVersion: "1",
      code: 404,
      message: `No dataset named "${urlName}" in namespace "${namespace.urlName}".`,
    }
    return
  }
  ctx.dataset = datasets[0]

  await next()
}


function toDatasetJson(dataset, urlNamespace, {showEventKey = false} = {}) {
  let datasetJson = {...dataset}
  datasetJson.createdAt = datasetJson.createdAt.toISOString()
  if (!showEventKey) delete datasetJson.eventKey
  delete datasetJson.id
  delete datasetJson.namespaceId
  datasetJson.urlNamespace = urlNamespace
  return datasetJson
}
