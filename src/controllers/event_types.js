// Databrew -- A data flow brewer, logger & dashboard
// By: Emmanuel Raviart <emmanuel@raviart.com>
//
// Copyright (C) 2015 Emmanuel Raviart
// https://git.framasoft.org/databrew/databrew
//
// Databrew is free software; you can redistribute it and/or modify
// it under the terms of the GNU Affero General Public License as
// published by the Free Software Foundation, either version 3 of the
// License, or (at your option) any later version.
//
// Databrew is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Affero General Public License for more details.
//
// You should have received a copy of the GNU Affero General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.


import getRawBody from "raw-body"

import {r} from "../database"
import {ownsNamespace} from "../model"


export {del}
async function del(ctx) {
  // Delete an existing event type.
  let eventType = ctx.eventType

  if (!(await ownsNamespace(ctx, ctx.authenticatedUser, ctx.datasetNamespace))) {
    ctx.status = 403  // Forbidden
    ctx.body = {
      apiVersion: "1",
      code: 403,  // Forbidden
      message: "Authenticated user doesn't own event type.",
    }
    return
  }

  // Delete all events belonging to this event type.
  await r
    .table("events")
    .getAll(eventType.id, {index: "typeId"})
    .delete()

  await r
    .table("eventTypes")
    .get(eventType.id)
    .delete()
  ctx.body = {
    apiVersion: "1",
    data: toEventTypeJson(eventType, ctx.dataset, ctx.datasetNamespace.urlName),
  }
}


export {deleteAll}
async function deleteAll(ctx) {
  // Delete all event types from a dataset.
  let dataset = ctx.dataset

  if (!(await ownsNamespace(ctx, ctx.authenticatedUser, ctx.datasetNamespace))) {
    ctx.status = 403  // Forbidden
    ctx.body = {
      apiVersion: "1",
      code: 403,  // Forbidden
      message: "Authenticated user doesn't own event type.",
    }
    return
  }

  // Delete all events.
  let eventTypes = await r
    .table("eventTypes")
    .getAll(dataset.id, {index: "datasetId"})
  for (let eventType of eventTypes) {
    await r
      .table("events")
      .getAll(eventType.id, {index: "typeId"})
      .delete()
  }

  // Delete all event types.
  await r
    .table("eventTypes")
    .getAll(dataset.id, {index: "datasetId"})
    .delete()
  ctx.body = {
    apiVersion: "1",
    message: `Event types of dataset "${ctx.datasetNamespace.urlName}/${dataset.urlName}" have been deleted.`,
  }
}


export {deleteEvents}
async function deleteEvents(ctx) {
  // Delete all events belonging to event type.
  let dataset = ctx.dataset
  let eventType = ctx.eventType

  if (!(await ownsNamespace(ctx, ctx.authenticatedUser, ctx.datasetNamespace))) {
    ctx.status = 403  // Forbidden
    ctx.body = {
      apiVersion: "1",
      code: 403,  // Forbidden
      message: "Authenticated user doesn't own event type.",
    }
    return
  }

  await r
    .table("events")
    .getAll(eventType.id, {index: "typeId"})
    .delete()
  ctx.body = {
    apiVersion: "1",
    message: `Events of "${ctx.datasetNamespace.urlName}/${dataset.urlName}/${eventType.urlName}" have been deleted.`,
  }
}


export {get}
async function get(ctx) {
  // Respond an existing eventType.
  ctx.body = {
    apiVersion: "1",
    data: toEventTypeJson(ctx.eventType, ctx.dataset, ctx.datasetNamespace.urlName),
  }
}


export {list}
async function list(ctx) {
  // Respond a list of all event types of a dataset.
  let dataset = ctx.dataset

  let eventTypes = await r
    .table("eventTypes")
    .getAll(dataset.id, {index: "datasetId"})
    .orderBy(r.desc("urlName"))
  ctx.body = {
    apiVersion: "1",
    data: eventTypes.map(eventType => ({
      dataset: {
        urlName: dataset.urlName,
        urlNamespace: ctx.datasetNamespace.urlName,
      },
      urlName: eventType.urlName,
    })),
  }
}


export {listEvents}
async function listEvents(ctx) {
  // Respond a list of all events of a dataset.
  let dataset = ctx.dataset
  let eventType = ctx.eventType

  let events = await r
    .table("events")
    .getAll(eventType.id, {index: "typeId"})
    .orderBy(r.desc("createdAt"))
  ctx.body = {
    apiVersion: "1",
    data: events.map(event => ({
      createdAt: event.createdAt.toISOString(),
      type: {
        dataset: {
          urlName: dataset.urlName,
          urlNamespace: ctx.datasetNamespace.urlName,
        },
        urlName: eventType.urlName,
      },
    })),
  }
}


export {notify}
async function notify(ctx) {
  // Notify a dataset of an event.
  let body = (await getRawBody(ctx.req, {
    length: ctx.length,
    limit: "1mb",
    encoding: ctx.charset,
  })).toString()
  let dataset = ctx.dataset
  let eventTypeUrlName = ctx.parameter.eventTypeName
  let namespace = ctx.datasetNamespace

  let eventTypes = await r
    .table("eventTypes")
    .getAll([dataset.id, eventTypeUrlName], {index: "datasetIdAndUrlName"})
    .limit(1)
  let eventType
  if (eventTypes.length < 1) {
    // Create event type.
    eventType = {
      createdAt: r.now(),
      datasetId: dataset.id,
      name: eventTypeUrlName,  // TODO: Use clean ctx.parameter.eventTypeName instead of urlName.
      urlName: eventTypeUrlName,
    }
    let result = await r
      .table("eventTypes")
      .insert(eventType, {returnChanges: true})
    eventType = result.changes[0].new_val
  } else {
    eventType = eventTypes[0]
  }

  let event = {
    body: body,
    createdAt: r.now(),
    headers: ctx.headers,
    method: ctx.method,
    querystring: ctx.querystring,
    remoteAddress: ctx.req.connection.remoteAddress,
    remotePort: ctx.req.connection.remotePort,
    typeId: eventType.id,
  }
  // TODO; Add request attributes to event (for CGI...).
  let result = await r
    .table("events")
    .insert(event, {returnChanges: true})
  event = result.changes[0].new_val
  ctx.body = {
    apiVersion: "1",
    message: `Dataset "${namespace.urlName}/${dataset.urlName}" has been notified.`,
    // data: toEventJson(event, dataset, ctx.datasetNamespace.urlName),
  }
}


export {requireEventType}
async function requireEventType(ctx, next) {
  let dataset = ctx.dataset
  let urlName = ctx.parameter.eventTypeName
  let eventTypes = await r
    .table("eventTypes")
    .getAll([dataset.id, urlName], {index: "datasetIdAndUrlName"})
    .limit(1)
  if (eventTypes.length < 1) {
    ctx.status = 404
    ctx.body = {
      apiVersion: "1",
      code: 404,
      message: `No event type with namespace "${ctx.datasetNamespace.urlName}", dataset "${dataset.urlName}"` +
        ` & urlName "${urlName}".`,
    }
    return
  }
  ctx.eventType = eventTypes[0]
  await next()
}


function toEventTypeJson(eventType, dataset, urlNamespace) {
  let eventTypeJson = {...eventType}
  eventTypeJson.createdAt = eventTypeJson.createdAt.toISOString()
  delete eventTypeJson.datasetId
  delete eventTypeJson.id
  eventTypeJson.dataset = {
    urlName: dataset.urlName,
    urlNamespace: urlNamespace,
  }
  return eventTypeJson
}
