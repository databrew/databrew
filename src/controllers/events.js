// Databrew -- A data flow brewer, logger & dashboard
// By: Emmanuel Raviart <emmanuel@raviart.com>
//
// Copyright (C) 2015 Emmanuel Raviart
// https://git.framasoft.org/databrew/databrew
//
// Databrew is free software; you can redistribute it and/or modify
// it under the terms of the GNU Affero General Public License as
// published by the Free Software Foundation, either version 3 of the
// License, or (at your option) any later version.
//
// Databrew is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Affero General Public License for more details.
//
// You should have received a copy of the GNU Affero General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.


import {r} from "../database"
import {ownsNamespace} from "../model"


export {del}
async function del(ctx) {
  // Delete an existing event.
  let event = ctx.event

  if (!(await ownsNamespace(ctx, ctx.authenticatedUser, ctx.datasetNamespace))) {
    ctx.status = 403  // Forbidden
    ctx.body = {
      apiVersion: "1",
      code: 403,  // Forbidden
      message: "Authenticated user doesn't own event.",
    }
    return
  }

  await r
    .table("events")
    .get(event.id)
    .delete()
  ctx.body = {
    apiVersion: "1",
    data: toEventJson(event, ctx.eventType, ctx.dataset, ctx.datasetNamespace.urlName),
  }
}


export {get}
async function get(ctx) {
  // Respond an existing event.
  ctx.body = {
    apiVersion: "1",
    data: toEventJson(ctx.event, ctx.eventType, ctx.dataset, ctx.datasetNamespace.urlName),
  }
}


export {redo}
async function redo(ctx) {
  // Duplicate an existing event, to notify it again.
  if (!(await ownsNamespace(ctx, ctx.authenticatedUser, ctx.datasetNamespace))) {
    ctx.status = 403  // Forbidden
    ctx.body = {
      apiVersion: "1",
      code: 403,  // Forbidden
      message: "Authenticated user doesn't own event.",
    }
    return
  }

  let event = {...ctx.event}
  event.createdAt = r.now()
  delete event.id
  delete event.status
  let result = await r
    .table("events")
    .insert(event, {returnChanges: true})
  event = result.changes[0].new_val
  ctx.status = 201  // Created
  ctx.body = {
    apiVersion: "1",
    data: toEventJson(event, ctx.eventType, ctx.dataset, ctx.datasetNamespace.urlName),
  }
}


export {requireEvent}
async function requireEvent(ctx, next) {
  let createdAt = await r.ISO8601(ctx.parameter.createdAt)
  let dataset = ctx.dataset
  let eventType = ctx.eventType
  let events = await r
    .table("events")
    .getAll([eventType.id, createdAt], {index: "typeIdAndCreatedAt"})
    .limit(1)
  if (events.length < 1) {
    ctx.status = 404
    ctx.body = {
      apiVersion: "1",
      code: 404,
      message: `No event with namespace "${ctx.datasetNamespace.urlName}", dataset "${dataset.urlName}",` +
        ` type "${eventType.urlName}" & createdAt "${createdAt.toISOString()}".`,
    }
    return
  }
  ctx.event = events[0]
  await next()
}


function toEventJson(event, eventType, dataset, urlNamespace) {
  let eventJson = {...event}
  eventJson.createdAt = eventJson.createdAt.toISOString()
  delete eventJson.id
  eventJson.type = {
    dataset: {
      urlName: dataset.urlName,
      urlNamespace: urlNamespace,
    },
    urlName: eventType.urlName,
  }
  delete eventJson.typeId
  return eventJson
}
