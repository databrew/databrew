// Databrew -- A data flow brewer, logger & dashboard
// By: Emmanuel Raviart <emmanuel@raviart.com>
//
// Copyright (C) 2015 Emmanuel Raviart
// https://git.framasoft.org/databrew/databrew
//
// Databrew is free software; you can redistribute it and/or modify
// it under the terms of the GNU Affero General Public License as
// published by the Free Software Foundation, either version 3 of the
// License, or (at your option) any later version.
//
// Databrew is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Affero General Public License for more details.
//
// You should have received a copy of the GNU Affero General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.



import {randomBytes} from "mz/crypto"
import getRawBody from "raw-body"

import {r} from "../database"
import {ownsNamespace} from "../model"


export function checkKey(require) {
  return async function checkKey(ctx, next) {
    let key = ctx.parameter["databrew-job-key"]
    if (key) {
      let jobs = await r
        .table("jobs")
        .getAll(key, {index: "key"})
        .limit(1)
      if (jobs.length < 1) {
        ctx.status = 403  // Forbidden
        ctx.body = {
          apiVersion: "1",
          code: 403,  // Forbidden
          message: `No job with key "${key}".`,
        }
        return
      }
      ctx.job = jobs[0]
    } else if (require) {
      ctx.status = 401  // Unauthorized
      ctx.body = {
        apiVersion: "1",
        code: 401,  // Unauthorized
        message: "Databrew-Job-Key header is required.",
      }
      return
    }
    await next()
  }
}


export {create}
async function create(ctx) {
  // Create a new job.
  let action = ctx.action
  let actionKey = ctx.actionKey
  let authenticatedUser = ctx.authenticatedUser
  let body = (await getRawBody(ctx.req, {
    length: ctx.length,
    limit: "1mb",
    encoding: ctx.charset,
  })).toString()

  if (!actionKey) {
    if (!authenticatedUser) {
      ctx.status = 401  // Unauthorized
      ctx.body = {
        apiVersion: "1",
        code: 401,  // Unauthorized
        message: "Either authentication or Databrew-Action-Key header is required.",
      }
      return
    }
    if (!(await ownsNamespace(ctx, authenticatedUser, ctx.actionNamespace))) {
      ctx.status = 403  // Forbidden
      ctx.body = {
        apiVersion: "1",
        code: 403,  // Forbidden
        message: "Authenticated user can't create job for this action.",
      }
      return
    }
  }

  let job = {
    actionId: action.id,
    commands: [],
    createdAt: r.now(),
    // eventId: event.id,  // There is no eventId when job is created witout event.
    key: (await randomBytes(16)).toString("hex"),  // 128 bits
    request: {
      body: body,
      headers: ctx.headers,
      method: ctx.method,
      querystring: ctx.querystring,
      remoteAddress: ctx.req.connection.remoteAddress,
      remotePort: ctx.req.connection.remotePort,
    },
  }
  let result = await r
    .table("jobs")
    .insert(job, {returnChanges: true})
  job = result.changes[0].new_val
  ctx.status = 201  // Created
  ctx.body = {
    apiVersion: "1",
    data: toJobJson(job, action, ctx.actionNamespace.urlName, null, null, null, {showKey: true}),
  }
}


export {del}
async function del(ctx) {
  // Delete an existing job.
  let job = ctx.job

  if (!(await ownsNamespace(ctx, ctx.authenticatedUser, ctx.actionNamespace))) {
    ctx.status = 403  // Forbidden
    ctx.body = {
      apiVersion: "1",
      code: 403,  // Forbidden
      message: "Authenticated user doesn't own job.",
    }
    return
  }

  await r
    .table("jobs")
    .get(job.id)
    .delete()

  let dataset
  let datasetNamespace
  let event = await r
    .table("events")
    .get(job.eventId)
  let eventType
  if (event) {
    eventType = await r
      .table("eventTypes")
      .get(event.typeId)
    dataset = await r
      .table("datasets")
      .get(eventType.datasetId)
    datasetNamespace = await r
      .table("namespaces")
      .get(dataset.namespaceId)
  }

  ctx.body = {
    apiVersion: "1",
    data: toJobJson(job, ctx.action, ctx.actionNamespace.urlName, event, eventType, dataset, datasetNamespace),
  }
}


export {get}
async function get(ctx) {
  // Respond an existing job.
  let job = ctx.job
  let show = ctx.parameter.show || []
  let showKey = show.includes("key")
  // TODO: Add security checks for key. See user.apiKey for example.

  let dataset
  let datasetNamespace
  let event = job.eventId ? await r
    .table("events")
    .get(job.eventId) : null
  let eventType
  if (event) {
    eventType = await r
      .table("eventTypes")
      .get(event.typeId)
    dataset = await r
      .table("datasets")
      .get(eventType.datasetId)
    datasetNamespace = await r
      .table("namespaces")
      .get(dataset.namespaceId)
  }

  ctx.body = {
    apiVersion: "1",
    data: toJobJson(job, ctx.action, ctx.actionNamespace.urlName, event, eventType, dataset, datasetNamespace,
      {showKey}),
  }
}


// export {list}
// async function list() {
//   // Respond a list of all jobs.
//   let jobs = await r
//     .table("jobs")
//     .orderBy({index: r.desc("createdAt")})
//   ctx.body = {
//     apiVersion: "1",
//     data: [
//       for (job of jobs)
//       {
//         createdAt: job.createdAt.toISOString(),
//         urlName: action.urlName,
//         urlNamespace: ctx.actionNamespace.urlName,
//       }
//     ],
//   }
// }


export {redo}
async function redo(ctx) {
  // Duplicate an existing job, to process it again.
  if (!(await ownsNamespace(ctx, ctx.authenticatedUser, ctx.actionNamespace))) {
    ctx.status = 403  // Forbidden
    ctx.body = {
      apiVersion: "1",
      code: 403,  // Forbidden
      message: "Authenticated user doesn't own job.",
    }
    return
  }

  let action = ctx.action
  let existingJob = ctx.job
  let job = {
    actionId: action.id,
    commands: [],
    createdAt: r.now(),
    eventId: existingJob.eventId,  // There is no eventId when duplicated job was created witout event.
    key: (await randomBytes(16)).toString("hex"),  // 128 bits
    request: existingJob.request,
  }
  let result = await r
    .table("jobs")
    .insert(job, {returnChanges: true})
  job = result.changes[0].new_val
  ctx.status = 201  // Created
  ctx.body = {
    apiVersion: "1",
    data: toJobJson(job, action, ctx.actionNamespace.urlName, null, null, null, {showKey: true}),
  }
}


export {requireJob}
async function requireJob(ctx, next) {
  let action = ctx.action

  let createdAt
  if (ctx.parameter) {
    createdAt = ctx.parameter.createdAt  // koa-swager
  } else {
    // koa-validate
    ctx.checkParams("createdAt")
      .notEmpty()
    createdAt = ctx.params.createdAt
  }
  createdAt = await r.ISO8601(createdAt)
  let jobs = await r
    .table("jobs")
    .getAll([action.id, createdAt], {index: "actionIdAndCreatedAt"})
    .limit(1)
  if (jobs.length < 1) {
    ctx.status = 404
    ctx.body = {
      apiVersion: "1",
      code: 404,
      message: `No job with namespace "${ctx.actionNamespace.urlName}", action "${action.urlName}"` +
        ` & createdAt "${createdAt.toISOString()}".`,
    }
    return
  }
  ctx.job = jobs[0]
  await next()
}


export {requireLatestJob}
async function requireLatestJob(ctx, next) {
  let action = ctx.action

  let jobs = await r
    .table("jobs")
    .getAll(action.id, {index: "actionId"})
    .orderBy(r.desc("createdAt"))
    .limit(1)
  if (jobs.length < 1) {
    ctx.status = 404
    ctx.body = {
      apiVersion: "1",
      code: 404,
      message: `No job with namespace "${ctx.actionNamespace.urlName}", action "${action.urlName}".`,
    }
    return
  }
  ctx.job = jobs[0]
  await next()
}


function toJobJson(job, action, urlNamespace, event, eventType, dataset, datasetNamespace, {showKey = false} = {}) {
  let jobJson = {...job}
  jobJson.action = {
    urlName: action.urlName,
    urlNamespace: urlNamespace,
  }
  delete jobJson.actionId
  jobJson.createdAt = jobJson.createdAt.toISOString()
  if (event) {
    jobJson.event = {
      createdAt: event.createdAt.toISOString(),
      type: {
        dataset: {
          urlName: dataset.urlName,
          urlNamespace: datasetNamespace.urlName,
        },
        urlName: eventType.urlName,
      },
    }
  }
  delete jobJson.eventId
  if (!showKey) delete jobJson.key
  delete jobJson.id
  return jobJson
}
