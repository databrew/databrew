// Databrew -- A data flow brewer, logger & dashboard
// By: Emmanuel Raviart <emmanuel@raviart.com>
//
// Copyright (C) 2015 Emmanuel Raviart
// https://git.framasoft.org/databrew/databrew
//
// Databrew is free software; you can redistribute it and/or modify
// it under the terms of the GNU Affero General Public License as
// published by the Free Software Foundation, either version 3 of the
// License, or (at your option) any later version.
//
// Databrew is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Affero General Public License for more details.
//
// You should have received a copy of the GNU Affero General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.


import {r} from "../database"


export function requireNamespace(paramName) {
  return async function requireNamespace(ctx, next) {
    let urlName
    if (ctx.parameter) {
      urlName = ctx.parameter[paramName]  // koa-swager
    } else {
      // koa-validate
      ctx.checkParams(paramName)
        .notEmpty()
      urlName = ctx.params[paramName]
    }
    let namespaces = await r
      .table("namespaces")
      .getAll(urlName, {index: "urlName"})
      .limit(1)
    if (namespaces.length < 1) {
      ctx.status = 404
      ctx.body = {
        apiVersion: "1",
        code: 404,
        message: `No namespace with urlName "${urlName}".`,
      }
      return
    }
    ctx[paramName] = namespaces[0]
    await next()
  }
}
