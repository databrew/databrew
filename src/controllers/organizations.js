// Databrew -- A data flow brewer, logger & dashboard
// By: Emmanuel Raviart <emmanuel@raviart.com>
//
// Copyright (C) 2015 Emmanuel Raviart
// https://git.framasoft.org/databrew/databrew
//
// Databrew is free software; you can redistribute it and/or modify
// it under the terms of the GNU Affero General Public License as
// published by the Free Software Foundation, either version 3 of the
// License, or (at your option) any later version.
//
// Databrew is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Affero General Public License for more details.
//
// You should have received a copy of the GNU Affero General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.


import {r} from "../database"
import {ownsOrganization} from "../model"


export {create}
async function create(ctx) {
  // Create a new organization.
  let organization = ctx.parameter.organization

  organization.createdAt = r.now()
  delete organization.id
  if (!organization.name) organization.name = organization.urlName

  let namespace = {
    kind: "organization",
    urlName: organization.urlName,
  }
  let namespaceResult = await r
    .table("namespaces")
    .insert(namespace, {returnChanges: true})
  namespace = namespaceResult.changes[0].new_val

  let result = await r
    .table("organizations")
    .insert(organization, {returnChanges: true})
  organization = result.changes[0].new_val

  let authenticatedUser = ctx.authenticatedUser
  await r
    .table("users")
    .get(authenticatedUser.id)
    .update({
      organizationsId: r.branch(
        r.row.hasFields("organizationsId"),
        r.row("organizationsId"),
        [],
      ).append(organization.id),
    })

  ctx.status = 201  // Created
  ctx.body = {
    apiVersion: "1",
    data: await toOrganizationJson(organization),
  }
}


export {del}
async function del(ctx) {
  // Delete an existing organization.
  let authenticatedUser = ctx.authenticatedUser
  let organization = ctx.organization

  if (!ownsOrganization(authenticatedUser, organization)) {
    ctx.status = 403  // Forbidden
    ctx.body = {
      apiVersion: "1",
      code: 403,  // Forbidden
      message: "A organization can only be deleted by one of its members or an admin.",
    }
    return
  }

  await r
    .table("users")
    .getAll(organization.id, {index: "organizationsId"})
    .update({
      organizationsId: r.row("organizationsId").difference([organization.id]),
    })

  // TODO: Delete datasets and actions?
  await r
    .table("namespaces")
    .getAll(organization.urlName, {index: "urlName"})
    .delete()
  await r
    .table("organizations")
    .get(organization.id)
    .delete()
  ctx.body = {
    apiVersion: "1",
    data: await toOrganizationJson(organization),
  }
}


export {get}
async function get(ctx) {
  // Respond an existing organization.
  // let authenticatedUser = ctx.authenticatedUser
  let show = ctx.parameter.show || []
  let showMembers = show.includes("members")
  let organization = ctx.organization
  // if (showMembers && !ownsOrganization(authenticatedUser, organization)) {
  //   ctx.status = 403  // Forbidden
  //   ctx.body = {
  //     apiVersion: "1",
  //     code: 403,  // Forbidden
  //     message: "Organization's members can only be viewed by its members or an admin.",
  //   }
  //   return
  // }
  ctx.body = {
    apiVersion: "1",
    data: await toOrganizationJson(organization, {showMembers}),
  }
}


export {listUrlNames}
async function listUrlNames(ctx) {
  // Respond a list of the urlNames of all organizations.
  let organizationsUrlName = await r
    .table("organizations")
    .orderBy({index: r.desc("createdAt")})
    .getField("urlName")
  ctx.body = {
    apiVersion: "1",
    data: organizationsUrlName,
  }
}


export {requireOrganization}
async function requireOrganization(ctx, next) {
  let urlName = ctx.parameter.organizationName
  let organizations = await r
    .table("organizations")
    .getAll(urlName, {index: "urlName"})
    .limit(1)
  if (organizations.length < 1) {
    ctx.status = 404
    ctx.body = {
      apiVersion: "1",
      code: 404,
      message: `No organization named "${urlName}".`,
    }
    return
  }
  ctx.organization = organizations[0]

  await next()
}


async function toOrganizationJson(organization, {showMembers = false} = {}) {
  let organizationJson = {...organization}
  organizationJson.createdAt = organizationJson.createdAt.toISOString()
  delete organizationJson.id
  if (showMembers) {
    organizationJson.members = await r
      .table("users")
      .getAll(organization.id, {index: "organizationsId"})
      .orderBy("urlName")
      .getField("urlName")
  }
  return organizationJson
}
