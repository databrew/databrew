// Databrew -- A data flow brewer, logger & dashboard
// By: Emmanuel Raviart <emmanuel@raviart.com>
//
// Copyright (C) 2015 Emmanuel Raviart
// https://git.framasoft.org/databrew/databrew
//
// Databrew is free software; you can redistribute it and/or modify
// it under the terms of the GNU Affero General Public License as
// published by the Free Software Foundation, either version 3 of the
// License, or (at your option) any later version.
//
// Databrew is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Affero General Public License for more details.
//
// You should have received a copy of the GNU Affero General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.


import config from "../config"


export const SPEC = {
  swagger: "2.0",
  info: {
    title: config.title,
    description: config.description,
    // termsOfService: "http://api.databrew.ovh/terms",
    contact: config.contact,
    license: config.license,
    version: "1",
  },
  host: [80, 443].includes(config.port) ? config.host.toString() : `${config.host}:${config.port}`,
  // basePath: "",
  // schemes: ["http", "https", "ws", "wss"],
  consumes: ["application/json"],
  produces: ["application/json"],
  paths: {
    "/actions": {
      get: {
        tags: ["action"],
        summary: "List actions",
        // description: "",
        // externalDocs: {},
        operationId: "actions.list",
        // consumes: ["application/json"],
        // produces: ["application/json"],
        parameters: [],
        responses: {
          "200": {
            description: "A wrapper containing references to actions",
            schema: {
              type: "object",
              properties: {
                apiVersion: {
                  type: "string",
                },
                data: {
                  type: "array",
                  items: {
                    $ref: "#/definitions/ActionReference",
                  },
                },
              },
              required: [
                "apiVersion",
                "data",
              ],
            },
          },
          default: {
            description: "Error payload",
            schema: {
              $ref: "#/definitions/Error",
            },
          },
        },
        // deprecated: true,
        // schemes: ["http", "https", "ws", "wss"],
        // security: {},
      },
      post: {
        tags: ["action"],
        summary: "Create a new action",
        // description: "",
        // externalDocs: {},
        operationId: "actions.create",
        // consumes: ["application/json"],
        // produces: ["application/json"],
        parameters: [
          {
            $ref: "#/parameters/actionParam",
          },
          {
            $ref: "#/parameters/apiKeyRequiredParam",
          },
        ],
        responses: {
          "201": {
            description: "A wrapper containing created action",
            schema: {
              type: "object",
              properties: {
                apiVersion: {
                  type: "string",
                },
                data: {
                  $ref: "#/definitions/Action",
                },
              },
              required: [
                "apiVersion",
                "data",
              ],
            },
          },
          default: {
            description: "Error payload",
            schema: {
              $ref: "#/definitions/Error",
            },
          },
        },
        // deprecated: true,
        // schemes: ["http", "https", "ws", "wss"],
        // security: [{apiKey: []}, {basic: []}],
      },
    },
    "/actions/{actionNamespace}": {
      get: {
        tags: ["action"],
        summary: "List actions of given namespace",
        // description: "",
        // externalDocs: {},
        operationId: "actions.listNamespace",
        // consumes: ["application/json"],
        // produces: ["application/json"],
        parameters: [
          {
            $ref: "#/parameters/actionNamespaceParam",
          },
        ],
        responses: {
          "200": {
            description: "A wrapper containing references to actions",
            schema: {
              type: "object",
              properties: {
                apiVersion: {
                  type: "string",
                },
                data: {
                  type: "array",
                  items: {
                    $ref: "#/definitions/ActionReference",
                  },
                },
              },
              required: [
                "apiVersion",
                "data",
              ],
            },
          },
          default: {
            description: "Error payload",
            schema: {
              $ref: "#/definitions/Error",
            },
          },
        },
        // deprecated: true,
        // schemes: ["http", "https", "ws", "wss"],
        // security: {},
      },
    },
    "/actions/{actionNamespace}/{actionName}": {
      delete: {
        tags: ["action"],
        summary: "Delete an existing action",
        // description: "",
        // externalDocs: {},
        operationId: "actions.del",
        // consumes: ["application/json"],
        // produces: ["application/json"],
        parameters: [
          {
            $ref: "#/parameters/actionNamespaceParam",
          },
          {
            $ref: "#/parameters/actionNameParam",
          },
          {
            $ref: "#/parameters/apiKeyRequiredParam",
          },
        ],
        responses: {
          "200": {
            description: "A wrapper containing the deleted action",
            schema: {
              type: "object",
              properties: {
                apiVersion: {
                  type: "string",
                },
                data: {
                  $ref: "#/definitions/Action",
                },
              },
              required: [
                "apiVersion",
                "data",
              ],
            },
          },
          default: {
            description: "Error payload",
            schema: {
              $ref: "#/definitions/Error",
            },
          },
        },
        // deprecated: true,
        // schemes: ["http", "https", "ws", "wss"],
        // security: [{apiKey: []}, {basic: []}],
      },
      get: {
        tags: ["action"],
        summary: "Get an action",
        // description: "",
        // externalDocs: {},
        operationId: "actions.get",
        // consumes: ["application/json"],
        // produces: ["application/json"],
        parameters: [
          {
            $ref: "#/parameters/actionNamespaceParam",
          },
          {
            $ref: "#/parameters/actionNameParam",
          },
          {
            $ref: "#/parameters/showParam",
          },
        ],
        responses: {
          "200": {
            description: "A wrapper containing the requested action",
            schema: {
              type: "object",
              properties: {
                apiVersion: {
                  type: "string",
                },
                data: {
                  $ref: "#/definitions/Action",
                },
              },
              required: [
                "apiVersion",
                "data",
              ],
            },
          },
          default: {
            description: "Error payload",
            schema: {
              $ref: "#/definitions/Error",
            },
          },
        },
        // deprecated: true,
        // schemes: ["http", "https", "ws", "wss"],
        // security: {},
      },
      patch: {
        tags: ["action"],
        summary: "Update some attributes an existing action",
        // description: "",
        // externalDocs: {},
        operationId: "actions.patch",
        // consumes: ["application/json"],
        // produces: ["application/json"],
        parameters: [
          {
            $ref: "#/parameters/actionNamespaceParam",
          },
          {
            $ref: "#/parameters/actionNameParam",
          },
          {
            // description: "",
            in: "body",
            name: "patch",
            required: true,
            schema: {
              type: "object",
              properties: {
                description: {
                  type: "string",
                },
                mergePending: {
                  type: "boolean",
                },
                name: {
                  type: "string",
                },
                private: {
                  type: "object",
                  additionalProperties: {
                    type: "string",
                  },
                },
              },
              // required: [],
            },
          },
          {
            $ref: "#/parameters/apiKeyRequiredParam",
          },
        ],
        responses: {
          "200": {
            description: "A wrapper containing the updated action",
            schema: {
              type: "object",
              properties: {
                apiVersion: {
                  type: "string",
                },
                data: {
                  $ref: "#/definitions/Action",
                },
              },
              required: [
                "apiVersion",
                "data",
              ],
            },
          },
          default: {
            description: "Error payload",
            schema: {
              $ref: "#/definitions/Error",
            },
          },
        },
        // deprecated: true,
        // schemes: ["http", "https", "ws", "wss"],
        // security: [{apiKey: []}, {basic: []}],
      },
    },
    "/actions/{actionNamespace}/{actionName}/script": {
      get: {
        tags: ["action", "script"],
        summary: "Get the script of given action, as text",
        // description: "",
        // externalDocs: {},
        operationId: "actions.getScript",
        // consumes: ["application/json"],
        produces: ["text/plain"],
        parameters: [
          {
            $ref: "#/parameters/actionNamespaceParam",
          },
          {
            $ref: "#/parameters/actionNameParam",
          },
        ],
        responses: {
          "200": {
            description: "The text of the script",
            schema: {
              type: "string",
            },
          },
          default: {
            description: "Error payload",
            schema: {
              $ref: "#/definitions/Error",
            },
          },
        },
        // deprecated: true,
        // schemes: ["http", "https", "ws", "wss"],
        // security: {},
      },
      post: {
        tags: ["action", "script"],
        summary: "Set the script of the given action, as text",
        // description: "",
        // externalDocs: {},
        operationId: "actions.setScript",
        consumes: ["text/plain"],
        // produces: ["application/json"],
        parameters: [
          {
            $ref: "#/parameters/actionNamespaceParam",
          },
          {
            $ref: "#/parameters/actionNameParam",
          },
          {
            // description: "",
            in: "body",
            name: "script",
            required: true,
            schema: {
              type: "string",
            },
          },
          {
            $ref: "#/parameters/apiKeyRequiredParam",
          },
        ],
        responses: {
          "200": {
            description: "A wrapper containing the modified action",
            schema: {
              type: "object",
              properties: {
                apiVersion: {
                  type: "string",
                },
                data: {
                  type: "array",
                  items: {
                    $ref: "#/definitions/Action",
                  },
                },
              },
              required: [
                "apiVersion",
                "data",
              ],
            },
          },
          default: {
            description: "Error payload",
            schema: {
              $ref: "#/definitions/Error",
            },
          },
        },
        // deprecated: true,
        // schemes: ["http", "https", "ws", "wss"],
        // security: [{apiKey: []}, {basic: []}],
      },
    },
    "/actions/{actionNamespace}/{actionName}/sources": {
      get: {
        tags: ["action", "event"],
        summary: "List all types of events handled by action",
        // description: "",
        // externalDocs: {},
        operationId: "actions.listSources",
        // consumes: ["application/json"],
        // produces: ["application/json"],
        parameters: [
          {
            $ref: "#/parameters/actionNamespaceParam",
          },
          {
            $ref: "#/parameters/actionNameParam",
          },
        ],
        responses: {
          "200": {
            description: "A wrapper containing references to generated datasets",
            schema: {
              type: "object",
              properties: {
                apiVersion: {
                  type: "string",
                },
                data: {
                  type: "array",
                  items: {
                    $ref: "#/definitions/EventTypeReference",
                  },
                },
              },
              required: [
                "apiVersion",
                "data",
              ],
            },
          },
          default: {
            description: "Error payload",
            schema: {
              $ref: "#/definitions/Error",
            },
          },
        },
        // deprecated: true,
        // schemes: ["http", "https", "ws", "wss"],
        // security: {},
      },
    },
    "/actions/{actionNamespace}/{actionName}/sources/{datasetNamespace}/{datasetName}/{eventTypeName}": {
      delete: {
        tags: ["action", "event"],
        summary: "Remove an event type handled by action",
        // description: "",
        // externalDocs: {},
        operationId: "actions.removeSource",
        // consumes: ["application/json"],
        // produces: ["application/json"],
        parameters: [
          {
            $ref: "#/parameters/actionNamespaceParam",
          },
          {
            $ref: "#/parameters/actionNameParam",
          },
          {
            $ref: "#/parameters/datasetNamespaceParam",
          },
          {
            $ref: "#/parameters/datasetNameParam",
          },
          {
            $ref: "#/parameters/eventTypeNameParam",
          },
          {
            $ref: "#/parameters/apiKeyOptionalParam",
          },
          {
            $ref: "#/parameters/jobKeyOptionalParam",
          },
        ],
        responses: {
          "200": {
            description: "A wrapper containing the modified list of datasets generated by action",
            schema: {
              type: "object",
              properties: {
                apiVersion: {
                  type: "string",
                },
                data: {
                  type: "array",
                  items: {
                    $ref: "#/definitions/EventTypeReference",
                  },
                },
              },
              required: [
                "apiVersion",
                "data",
              ],
            },
          },
          default: {
            description: "Error payload",
            schema: {
              $ref: "#/definitions/Error",
            },
          },
        },
        // deprecated: true,
        // schemes: ["http", "https", "ws", "wss"],
        // security: [{apiKey: []}, {basic: []}],
      },
      post: {
        tags: ["action", "event"],
        summary: "Add (if needed) an event type handled by action and notify action of event",
        description: "This function must be called by scripts when they have used a source dataset.",
        // externalDocs: {},
        operationId: "actions.registerAndNotifySource",
        // consumes: ["application/json"],
        // produces: ["application/json"],
        parameters: [
          {
            $ref: "#/parameters/actionNamespaceParam",
          },
          {
            $ref: "#/parameters/actionNameParam",
          },
          {
            $ref: "#/parameters/datasetNamespaceParam",
          },
          {
            $ref: "#/parameters/datasetNameParam",
          },
          {
            $ref: "#/parameters/eventTypeNameParam",
          },
          {
            $ref: "#/parameters/apiKeyOptionalParam",
          },
          {
            $ref: "#/parameters/jobKeyOptionalParam",
          },
        ],
        responses: {
          "200": {
            description: "A wrapper containing the modified list of datasets generated by action",
            schema: {
              type: "object",
              properties: {
                apiVersion: {
                  type: "string",
                },
                data: {
                  type: "array",
                  items: {
                    $ref: "#/definitions/EventTypeReference",
                  },
                },
              },
              required: [
                "apiVersion",
                "data",
              ],
            },
          },
          default: {
            description: "Error payload",
            schema: {
              $ref: "#/definitions/Error",
            },
          },
        },
        // deprecated: true,
        // schemes: ["http", "https", "ws", "wss"],
        // security: [{apiKey: []}, {basic: []}],
      },
    },
    "/actions/{actionNamespace}/{actionName}/targets": {
      get: {
        tags: ["action", "event"],
        summary: "List all types of events generated by action",
        // description: "",
        // externalDocs: {},
        operationId: "actions.listTargets",
        // consumes: ["application/json"],
        // produces: ["application/json"],
        parameters: [
          {
            $ref: "#/parameters/actionNamespaceParam",
          },
          {
            $ref: "#/parameters/actionNameParam",
          },
        ],
        responses: {
          "200": {
            description: "A wrapper containing references to generated datasets",
            schema: {
              type: "object",
              properties: {
                apiVersion: {
                  type: "string",
                },
                data: {
                  type: "array",
                  items: {
                    $ref: "#/definitions/EventTypeReference",
                  },
                },
              },
              required: [
                "apiVersion",
                "data",
              ],
            },
          },
          default: {
            description: "Error payload",
            schema: {
              $ref: "#/definitions/Error",
            },
          },
        },
        // deprecated: true,
        // schemes: ["http", "https", "ws", "wss"],
        // security: {},
      },
      post: {
        tags: ["action", "event"],
        summary: "Notify an action (ie all its targets) of an event",
        description: "Used by jobs (their scripts) to send a notification to an event.",
        // externalDocs: {},
        operationId: "actions.notifyTargets",
        // consumes: ["application/json"],
        // produces: ["application/json"],
        parameters: [
          {
            $ref: "#/parameters/actionNamespaceParam",
          },
          {
            $ref: "#/parameters/actionNameParam",
          },
          // No description for body (of event), because its structure is unknown.
          {
            $ref: "#/parameters/apiKeyOptionalParam",
          },
          {
            $ref: "#/parameters/jobKeyOptionalParam",
          },
        ],
        responses: {
          "200": {
            description: "A wrapper containing a notification message",
            schema: {
              type: "object",
              properties: {
                apiVersion: {
                  type: "string",
                },
                message: {
                  type: "string",
                },
              },
              required: [
                "apiVersion",
                "message",
              ],
            },
          },
          default: {
            description: "Error payload",
            schema: {
              $ref: "#/definitions/Error",
            },
          },
        },
        // deprecated: true,
        // schemes: ["http", "https", "ws", "wss"],
        // security: {},
      },
    },
    "/actions/{actionNamespace}/{actionName}/targets/{datasetNamespace}/{datasetName}/{eventTypeName}": {
      delete: {
        tags: ["action", "event"],
        summary: "Remove a type of event generated by action",
        // description: "",
        // externalDocs: {},
        operationId: "actions.removeTarget",
        // consumes: ["application/json"],
        // produces: ["application/json"],
        parameters: [
          {
            $ref: "#/parameters/actionNamespaceParam",
          },
          {
            $ref: "#/parameters/actionNameParam",
          },
          {
            $ref: "#/parameters/datasetNamespaceParam",
          },
          {
            $ref: "#/parameters/datasetNameParam",
          },
          {
            $ref: "#/parameters/eventTypeNameParam",
          },
          {
            $ref: "#/parameters/apiKeyOptionalParam",
          },
          {
            $ref: "#/parameters/jobKeyOptionalParam",
          },
        ],
        responses: {
          "200": {
            description: "A wrapper containing the modified list of datasets generated by action",
            schema: {
              type: "object",
              properties: {
                apiVersion: {
                  type: "string",
                },
                data: {
                  type: "array",
                  items: {
                    $ref: "#/definitions/EventTypeReference",
                  },
                },
              },
              required: [
                "apiVersion",
                "data",
              ],
            },
          },
          default: {
            description: "Error payload",
            schema: {
              $ref: "#/definitions/Error",
            },
          },
        },
        // deprecated: true,
        // schemes: ["http", "https", "ws", "wss"],
        // security: [{apiKey: []}, {basic: []}],
      },
      post: {
        tags: ["action", "event"],
        summary: "Add a target type of event to action (if needed) and notify it of an update event",
        description: "This function must be called by scripts when they have updated a dataset.",
        // externalDocs: {},
        operationId: "actions.registerAndNotifyTarget",
        // consumes: ["application/json"],
        // produces: ["application/json"],
        parameters: [
          {
            $ref: "#/parameters/actionNamespaceParam",
          },
          {
            $ref: "#/parameters/actionNameParam",
          },
          {
            $ref: "#/parameters/datasetNamespaceParam",
          },
          {
            $ref: "#/parameters/datasetNameParam",
          },
          {
            $ref: "#/parameters/eventTypeNameParam",
          },
          // No description for body (of event), because its structure is unknown.
          {
            $ref: "#/parameters/apiKeyOptionalParam",
          },
          {
            $ref: "#/parameters/jobKeyOptionalParam",
          },
        ],
        responses: {
          "200": {
            description: "A wrapper containing the modified list of datasets generated by action",
            schema: {
              type: "object",
              properties: {
                apiVersion: {
                  type: "string",
                },
                data: {
                  type: "array",
                  items: {
                    $ref: "#/definitions/EventTypeReference",
                  },
                },
              },
              required: [
                "apiVersion",
                "data",
              ],
            },
          },
          default: {
            description: "Error payload",
            schema: {
              $ref: "#/definitions/Error",
            },
          },
        },
        // deprecated: true,
        // schemes: ["http", "https", "ws", "wss"],
        // security: [{apiKey: []}, {basic: []}],
      },
    },
    "/datasets": {
      get: {
        tags: ["dataset"],
        summary: "List datasets",
        // description: "",
        // externalDocs: {},
        operationId: "datasets.list",
        // consumes: ["application/json"],
        // produces: ["application/json"],
        parameters: [],
        responses: {
          "200": {
            description: "A wrapper containing references to datasets",
            schema: {
              type: "object",
              properties: {
                apiVersion: {
                  type: "string",
                },
                data: {
                  type: "array",
                  items: {
                    $ref: "#/definitions/DatasetReference",
                  },
                },
              },
              required: [
                "apiVersion",
                "data",
              ],
            },
          },
          default: {
            description: "Error payload",
            schema: {
              $ref: "#/definitions/Error",
            },
          },
        },
        // deprecated: true,
        // schemes: ["http", "https", "ws", "wss"],
        // security: {},
      },
      post: {
        tags: ["dataset"],
        summary: "Create a new dataset",
        // description: "",
        // externalDocs: {},
        operationId: "datasets.create",
        // consumes: ["application/json"],
        // produces: ["application/json"],
        parameters: [
          {
            $ref: "#/parameters/datasetParam",
          },
          {
            $ref: "#/parameters/apiKeyRequiredParam",
          },
        ],
        responses: {
          "201": {
            description: "A wrapper containing the created dataset",
            schema: {
              type: "object",
              properties: {
                apiVersion: {
                  type: "string",
                },
                data: {
                  $ref: "#/definitions/Dataset",
                },
              },
              required: [
                "apiVersion",
                "data",
              ],
            },
          },
          default: {
            description: "Error payload",
            schema: {
              $ref: "#/definitions/Error",
            },
          },
        },
        // deprecated: true,
        // schemes: ["http", "https", "ws", "wss"],
        // security: [{apiKey: []}, {basic: []}],
      },
      // put: {
      //   tags: ["dataset"],
      //   summary: "Update an existing dataset",
      //   // description: "",
      //   // externalDocs: {},
      //   operationId: "datasets.update",
      //   // consumes: ["application/json"],
      //   // produces: ["application/json"],
      //   parameters: [
      //     {
      //       $ref: "#/parameters/datasetParam",
      //     },
      //     {
      //       $ref: "#/parameters/apiKeyRequiredParam",
      //     },
      //   ],
      //   responses: {
      //     "200": {
      //       description: "A wrapper containing the updated dataset",
      //       schema: {
      //         type: "object",
      //         properties: {
      //           apiVersion: {
      //             type: "string",
      //           },
      //           data: {
      //             $ref: "#/definitions/Dataset",
      //           },
      //         },
      //         required: [
      //           "apiVersion",
      //           "data",
      //         ],
      //       },
      //     },
      //     default: {
      //       description: "Error payload",
      //       schema: {
      //         $ref: "#/definitions/Error",
      //       },
      //     },
      //   },
      //   // deprecated: true,
      //   // schemes: ["http", "https", "ws", "wss"],
      //   // security: [{apiKey: []}, {basic: []}],
      // },
    },
    "/datasets/{datasetNamespace}": {
      get: {
        tags: ["dataset"],
        summary: "List datasets of given namespace",
        // description: "",
        // externalDocs: {},
        operationId: "datasets.listNamespace",
        // consumes: ["application/json"],
        // produces: ["application/json"],
        parameters: [
          {
            $ref: "#/parameters/datasetNamespaceParam",
          },
        ],
        responses: {
          "200": {
            description: "A wrapper containing the requested dataset",
            schema: {
              type: "object",
              properties: {
                apiVersion: {
                  type: "string",
                },
                data: {
                  type: "array",
                  items: {
                    $ref: "#/definitions/DatasetReference",
                  },
                },
              },
              required: [
                "apiVersion",
                "data",
              ],
            },
          },
          default: {
            description: "Error payload",
            schema: {
              $ref: "#/definitions/Error",
            },
          },
        },
        // deprecated: true,
        // schemes: ["http", "https", "ws", "wss"],
        // security: {},
      },
    },
    "/datasets/{datasetNamespace}/{datasetName}": {
      delete: {
        tags: ["dataset"],
        summary: "Delete an existing dataset",
        // description: "",
        // externalDocs: {},
        operationId: "datasets.del",
        // consumes: ["application/json"],
        // produces: ["application/json"],
        parameters: [
          {
            $ref: "#/parameters/datasetNamespaceParam",
          },
          {
            $ref: "#/parameters/datasetNameParam",
          },
          {
            $ref: "#/parameters/apiKeyRequiredParam",
          },
        ],
        responses: {
          "200": {
            description: "A wrapper containing the deleted dataset",
            schema: {
              type: "object",
              properties: {
                apiVersion: {
                  type: "string",
                },
                data: {
                  $ref: "#/definitions/Dataset",
                },
              },
              required: [
                "apiVersion",
                "data",
              ],
            },
          },
          default: {
            description: "Error payload",
            schema: {
              $ref: "#/definitions/Error",
            },
          },
        },
        // deprecated: true,
        // schemes: ["http", "https", "ws", "wss"],
        // security: [{apiKey: []}, {basic: []}],
      },
      get: {
        tags: ["dataset"],
        summary: "Get a dataset",
        // description: "",
        // externalDocs: {},
        operationId: "datasets.get",
        // consumes: ["application/json"],
        // produces: ["application/json"],
        parameters: [
          {
            $ref: "#/parameters/datasetNamespaceParam",
          },
          {
            $ref: "#/parameters/datasetNameParam",
          },
          {
            $ref: "#/parameters/showParam",
          },
        ],
        responses: {
          "200": {
            description: "A wrapper containing the requested dataset",
            schema: {
              type: "object",
              properties: {
                apiVersion: {
                  type: "string",
                },
                data: {
                  $ref: "#/definitions/Dataset",
                },
              },
              required: [
                "apiVersion",
                "data",
              ],
            },
          },
          default: {
            description: "Error payload",
            schema: {
              $ref: "#/definitions/Error",
            },
          },
        },
        // deprecated: true,
        // schemes: ["http", "https", "ws", "wss"],
        // security: {},
      },
      patch: {
        tags: ["dataset"],
        summary: "Update some attributes an existing dataset",
        // description: "",
        // externalDocs: {},
        operationId: "datasets.patch",
        // consumes: ["application/json"],
        // produces: ["application/json"],
        parameters: [
          {
            $ref: "#/parameters/datasetNamespaceParam",
          },
          {
            $ref: "#/parameters/datasetNameParam",
          },
          {
            // description: "",
            in: "body",
            name: "patch",
            required: true,
            schema: {
              type: "object",
              properties: {
                description: {
                  type: "string",
                },
                name: {
                  type: "string",
                },
                url: {
                  type: "string",
                  format: "url",
                },
              },
              // required: [],
            },
          },
          {
            $ref: "#/parameters/apiKeyRequiredParam",
          },
        ],
        responses: {
          "200": {
            description: "A wrapper containing the updated dataset",
            schema: {
              type: "object",
              properties: {
                apiVersion: {
                  type: "string",
                },
                data: {
                  $ref: "#/definitions/Dataset",
                },
              },
              required: [
                "apiVersion",
                "data",
              ],
            },
          },
          default: {
            description: "Error payload",
            schema: {
              $ref: "#/definitions/Error",
            },
          },
        },
        // deprecated: true,
        // schemes: ["http", "https", "ws", "wss"],
        // security: [{apiKey: []}, {basic: []}],
      },
    },
    "/datasets/{datasetNamespace}/{datasetName}/actions": {
      get: {
        tags: ["action", "dataset"],
        summary: "List all actions triggered by events occuring to a dataset",
        // description: "",
        // externalDocs: {},
        operationId: "datasets.listActions",
        // consumes: ["application/json"],
        // produces: ["application/json"],
        parameters: [
          {
            $ref: "#/parameters/datasetNamespaceParam",
          },
          {
            $ref: "#/parameters/datasetNameParam",
          },
        ],
        responses: {
          "200": {
            description: "A wrapper containing references to actions",
            schema: {
              type: "object",
              properties: {
                apiVersion: {
                  type: "string",
                },
                data: {
                  type: "array",
                  items: {
                    $ref: "#/definitions/ActionReference",
                  },
                },
              },
              required: [
                "apiVersion",
                "data",
              ],
            },
          },
          default: {
            description: "Error payload",
            schema: {
              $ref: "#/definitions/Error",
            },
          },
        },
        // deprecated: true,
        // schemes: ["http", "https", "ws", "wss"],
        // security: {},
      },
    },
    "/datasets/{datasetNamespace}/{datasetName}/events/{eventTypeName}": {
      delete: {
        tags: ["dataset", "event"],
        summary: "Delete all events of a given type from a dataset",
        // description: "",
        // externalDocs: {},
        operationId: "eventTypes.deleteEvents",
        // consumes: ["application/json"],
        // produces: ["application/json"],
        parameters: [
          {
            $ref: "#/parameters/datasetNamespaceParam",
          },
          {
            $ref: "#/parameters/datasetNameParam",
          },
          {
            $ref: "#/parameters/eventTypeNameParam",
          },
          {
            $ref: "#/parameters/apiKeyRequiredParam",
          },
        ],
        responses: {
          "200": {
            description: "A wrapper containing a deletion message",
            schema: {
              type: "object",
              properties: {
                apiVersion: {
                  type: "string",
                },
                message: {
                  type: "string",
                },
              },
              required: [
                "apiVersion",
                "message",
              ],
            },
          },
          default: {
            description: "Error payload",
            schema: {
              $ref: "#/definitions/Error",
            },
          },
        },
        // deprecated: true,
        // schemes: ["http", "https", "ws", "wss"],
        // security: [{apiKey: []}, {basic: []}],
      },
      get: {
        tags: ["dataset", "event"],
        summary: "List all events of a given type for a dataset",
        // description: "",
        // externalDocs: {},
        operationId: "eventTypes.listEvents",
        // consumes: ["application/json"],
        // produces: ["application/json"],
        parameters: [
          {
            $ref: "#/parameters/datasetNamespaceParam",
          },
          {
            $ref: "#/parameters/datasetNameParam",
          },
          {
            $ref: "#/parameters/eventTypeNameParam",
          },
        ],
        responses: {
          "200": {
            description: "A wrapper containing references to events",
            schema: {
              type: "object",
              properties: {
                apiVersion: {
                  type: "string",
                },
                data: {
                  type: "array",
                  items: {
                    $ref: "#/definitions/EventReference",
                  },
                },
              },
              required: [
                "apiVersion",
                "data",
              ],
            },
          },
          default: {
            description: "Error payload",
            schema: {
              $ref: "#/definitions/Error",
            },
          },
        },
        // deprecated: true,
        // schemes: ["http", "https", "ws", "wss"],
        // security: {},
      },
    },
    // "/events": {
    //   get: {
    //     tags: ["dataset", "event"],
    //     summary: "List pending events",
    //     // description: "",
    //     // externalDocs: {},
    //     operationId: "events.list",
    //     // consumes: ["application/json"],
    //     // produces: ["application/json"],
    //     parameters: [],
    //     responses: {
    //       "200": {
    //         description: "A wrapper containing references to events",
    //         schema: {
    //           type: "object",
    //           properties: {
    //             apiVersion: {
    //               type: "string",
    //             },
    //             data: {
    //               type: "array",
    //               items: {
    //                 $ref: "#/definitions/EventReference",
    //               },
    //             },
    //           },
    //           required: [
    //             "apiVersion",
    //             "data",
    //           ],
    //         },
    //       },
    //       default: {
    //         description: "Error payload",
    //         schema: {
    //           $ref: "#/definitions/Error",
    //         },
    //       },
    //     },
    //     // deprecated: true,
    //     // schemes: ["http", "https", "ws", "wss"],
    //     // security: {},
    //   },
    // },
    "/events/{datasetNamespace}/{datasetName}": {
      delete: {
        tags: ["dataset", "event"],
        summary: "Delete all event types from a dataset",
        // description: "",
        // externalDocs: {},
        operationId: "eventTypes.deleteAll",
        // consumes: ["application/json"],
        // produces: ["application/json"],
        parameters: [
          {
            $ref: "#/parameters/datasetNamespaceParam",
          },
          {
            $ref: "#/parameters/datasetNameParam",
          },
          {
            $ref: "#/parameters/apiKeyRequiredParam",
          },
        ],
        responses: {
          "200": {
            description: "A wrapper containing a deletion message",
            schema: {
              type: "object",
              properties: {
                apiVersion: {
                  type: "string",
                },
                message: {
                  type: "string",
                },
              },
              required: [
                "apiVersion",
                "message",
              ],
            },
          },
          default: {
            description: "Error payload",
            schema: {
              $ref: "#/definitions/Error",
            },
          },
        },
        // deprecated: true,
        // schemes: ["http", "https", "ws", "wss"],
        // security: [{apiKey: []}, {basic: []}],
      },
      get: {
        tags: ["dataset", "event"],
        summary: "List all event types of a dataset",
        // description: "",
        // externalDocs: {},
        operationId: "eventTypes.list",
        // consumes: ["application/json"],
        // produces: ["application/json"],
        parameters: [
          {
            $ref: "#/parameters/datasetNamespaceParam",
          },
          {
            $ref: "#/parameters/datasetNameParam",
          },
        ],
        responses: {
          "200": {
            description: "A wrapper containing references to event types",
            schema: {
              type: "object",
              properties: {
                apiVersion: {
                  type: "string",
                },
                data: {
                  type: "array",
                  items: {
                    $ref: "#/definitions/EventTypeReference",
                  },
                },
              },
              required: [
                "apiVersion",
                "data",
              ],
            },
          },
          default: {
            description: "Error payload",
            schema: {
              $ref: "#/definitions/Error",
            },
          },
        },
        // deprecated: true,
        // schemes: ["http", "https", "ws", "wss"],
        // security: {},
      },
    },
    "/events/{datasetNamespace}/{datasetName}/{eventTypeName}": {
      delete: {
        tags: ["dataset", "event"],
        summary: "Delete an event type from a dataset",
        // description: "",
        // externalDocs: {},
        operationId: "eventType.del",
        // consumes: ["application/json"],
        // produces: ["application/json"],
        parameters: [
          {
            $ref: "#/parameters/datasetNamespaceParam",
          },
          {
            $ref: "#/parameters/datasetNameParam",
          },
          {
            $ref: "#/parameters/eventTypeNameParam",
          },
          {
            $ref: "#/parameters/apiKeyRequiredParam",
          },
        ],
        responses: {
          "200": {
            description: "A wrapper containing the deleted event type",
            schema: {
              type: "object",
              properties: {
                apiVersion: {
                  type: "string",
                },
                data: {
                  $ref: "#/definitions/EventType",
                },
              },
              required: [
                "apiVersion",
                "data",
              ],
            },
          },
          default: {
            description: "Error payload",
            schema: {
              $ref: "#/definitions/Error",
            },
          },
        },
        // deprecated: true,
        // schemes: ["http", "https", "ws", "wss"],
        // security: [{apiKey: []}, {basic: []}],
      },
      get: {
        tags: ["dataset", "event"],
        summary: "Get an event type of a dataset",
        // description: "",
        // externalDocs: {},
        operationId: "eventTypes.get",
        // consumes: ["application/json"],
        // produces: ["application/json"],
        parameters: [
          {
            $ref: "#/parameters/datasetNamespaceParam",
          },
          {
            $ref: "#/parameters/datasetNameParam",
          },
          {
            $ref: "#/parameters/eventTypeNameParam",
          },
        ],
        responses: {
          "200": {
            description: "A wrapper containing the event type",
            schema: {
              type: "object",
              properties: {
                apiVersion: {
                  type: "string",
                },
                data: {
                  $ref: "#/definitions/EventType",
                },
              },
              required: [
                "apiVersion",
                "data",
              ],
            },
          },
          default: {
            description: "Error payload",
            schema: {
              $ref: "#/definitions/Error",
            },
          },
        },
        // deprecated: true,
        // schemes: ["http", "https", "ws", "wss"],
        // security: {},
      },
      post: {
        tags: ["dataset", "event"],
        summary: "Notify a dataset of an event",
        // description: "",
        // externalDocs: {},
        operationId: "eventTypes.notify",
        // consumes: ["application/json"],
        // produces: ["application/json"],
        parameters: [
          {
            $ref: "#/parameters/datasetNamespaceParam",
          },
          {
            $ref: "#/parameters/datasetNameParam",
          },
          {
            $ref: "#/parameters/eventTypeNameParam",
          },
          {
            $ref: "#/parameters/eventKeyQueryParam",
          },
          {
            $ref: "#/parameters/eventKeyHeaderParam",
          },
          // No description for body (of event), because its structure is unknown.
        ],
        responses: {
          "200": {
            description: "A wrapper containing a notification message",
            schema: {
              type: "object",
              properties: {
                apiVersion: {
                  type: "string",
                },
                message: {
                  type: "string",
                },
              },
              required: [
                "apiVersion",
                "message",
              ],
            },
          },
          default: {
            description: "Error payload",
            schema: {
              $ref: "#/definitions/Error",
            },
          },
        },
        // deprecated: true,
        // schemes: ["http", "https", "ws", "wss"],
        // security: {},
      },
    },
    "/events/{datasetNamespace}/{datasetName}/{eventTypeName}/{createdAt}": {
      delete: {
        tags: ["event"],
        summary: "Delete an existing event",
        // description: "",
        // externalDocs: {},
        operationId: "events.del",
        // consumes: ["application/json"],
        // produces: ["application/json"],
        parameters: [
          {
            $ref: "#/parameters/datasetNamespaceParam",
          },
          {
            $ref: "#/parameters/datasetNameParam",
          },
          {
            $ref: "#/parameters/eventTypeNameParam",
          },
          {
            $ref: "#/parameters/createdAtParam",
          },
          {
            $ref: "#/parameters/apiKeyRequiredParam",
          },
        ],
        responses: {
          "200": {
            description: "A wrapper containing the deleted event",
            schema: {
              type: "object",
              properties: {
                apiVersion: {
                  type: "string",
                },
                data: {
                  $ref: "#/definitions/Event",
                },
              },
              required: [
                "apiVersion",
                "data",
              ],
            },
          },
          default: {
            description: "Error payload",
            schema: {
              $ref: "#/definitions/Error",
            },
          },
        },
        // deprecated: true,
        // schemes: ["http", "https", "ws", "wss"],
        // security: [{apiKey: []}, {basic: []}],
      },
      get: {
        tags: ["event"],
        summary: "Get an existing event",
        // description: "",
        // externalDocs: {},
        operationId: "events.get",
        // consumes: ["application/json"],
        // produces: ["application/json"],
        parameters: [
          {
            $ref: "#/parameters/datasetNamespaceParam",
          },
          {
            $ref: "#/parameters/datasetNameParam",
          },
          {
            $ref: "#/parameters/eventTypeNameParam",
          },
          {
            $ref: "#/parameters/createdAtParam",
          },
        ],
        responses: {
          "200": {
            description: "A wrapper containing the requested event",
            schema: {
              type: "object",
              properties: {
                apiVersion: {
                  type: "string",
                },
                data: {
                  $ref: "#/definitions/Event",
                },
              },
              required: [
                "apiVersion",
                "data",
              ],
            },
          },
          default: {
            description: "Error payload",
            schema: {
              $ref: "#/definitions/Error",
            },
          },
        },
        // deprecated: true,
        // schemes: ["http", "https", "ws", "wss"],
        // security: {},
      },
    },
    "/events/{datasetNamespace}/{datasetName}/{eventTypeName}/{createdAt}/redo": {
      post: {
        tags: ["event"],
        summary: "Redo an existing event",
        // description: "",
        // externalDocs: {},
        operationId: "events.redo",
        // consumes: ["application/json"],
        // produces: ["application/json"],
        parameters: [
          {
            $ref: "#/parameters/datasetNamespaceParam",
          },
          {
            $ref: "#/parameters/datasetNameParam",
          },
          {
            $ref: "#/parameters/eventTypeNameParam",
          },
          {
            $ref: "#/parameters/createdAtParam",
          },
          {
            $ref: "#/parameters/apiKeyRequiredParam",
          },
        ],
        responses: {
          "201": {
            description: "A wrapper containing the created event",
            schema: {
              type: "object",
              properties: {
                apiVersion: {
                  type: "string",
                },
                data: {
                  $ref: "#/definitions/Event",
                },
              },
              required: [
                "apiVersion",
                "data",
              ],
            },
          },
          default: {
            description: "Error payload",
            schema: {
              $ref: "#/definitions/Error",
            },
          },
        },
        // deprecated: true,
        // schemes: ["http", "https", "ws", "wss"],
        // security: [{apiKey: []}, {basic: []}],
      },
    },
    "/jobs/{actionNamespace}/{actionName}": {
      delete: {
        tags: ["action", "job"],
        summary: "Delete all jobs from an action",
        // description: "",
        // externalDocs: {},
        operationId: "actions.deleteJobs",
        // consumes: ["application/json"],
        // produces: ["application/json"],
        parameters: [
          {
            $ref: "#/parameters/actionNamespaceParam",
          },
          {
            $ref: "#/parameters/actionNameParam",
          },
          {
            $ref: "#/parameters/apiKeyRequiredParam",
          },
        ],
        responses: {
          "200": {
            description: "A wrapper containing a deletion message",
            schema: {
              type: "object",
              properties: {
                apiVersion: {
                  type: "string",
                },
                message: {
                  type: "string",
                },
              },
              required: [
                "apiVersion",
                "message",
              ],
            },
          },
          default: {
            description: "Error payload",
            schema: {
              $ref: "#/definitions/Error",
            },
          },
        },
        // deprecated: true,
        // schemes: ["http", "https", "ws", "wss"],
        // security: [{apiKey: []}, {basic: []}],
      },
      get: {
        tags: ["action", "job"],
        summary: "List all jobs of an action",
        // description: "",
        // externalDocs: {},
        operationId: "actions.listJobs",
        // consumes: ["application/json"],
        // produces: ["application/json"],
        parameters: [
          {
            $ref: "#/parameters/actionNamespaceParam",
          },
          {
            $ref: "#/parameters/actionNameParam",
          },
        ],
        responses: {
          "200": {
            description: "A wrapper containing references to jobs",
            schema: {
              type: "object",
              properties: {
                apiVersion: {
                  type: "string",
                },
                data: {
                  type: "array",
                  items: {
                    $ref: "#/definitions/JobReference",
                  },
                },
              },
              required: [
                "apiVersion",
                "data",
              ],
            },
          },
          default: {
            description: "Error payload",
            schema: {
              $ref: "#/definitions/Error",
            },
          },
        },
        // deprecated: true,
        // schemes: ["http", "https", "ws", "wss"],
        // security: {},
      },
      post: {
        tags: ["action", "job"],
        summary: "Create a new job for action",
        // description: "",
        // externalDocs: {},
        operationId: "jobs.create",
        consumes: ["text/plain"],
        // produces: ["application/json"],
        parameters: [
          {
            $ref: "#/parameters/actionNamespaceParam",
          },
          {
            $ref: "#/parameters/actionNameParam",
          },
          // No description for body (of event), because its structure is unknown.
          {
            $ref: "#/parameters/apiKeyOptionalParam",
          },
          {
            $ref: "#/parameters/actionKeyOptionalParam",
          },
        ],
        responses: {
          "201": {
            description: "A wrapper containing created action",
            schema: {
              type: "object",
              properties: {
                apiVersion: {
                  type: "string",
                },
                data: {
                  $ref: "#/definitions/Job",
                },
              },
              required: [
                "apiVersion",
                "data",
              ],
            },
          },
          default: {
            description: "Error payload",
            schema: {
              $ref: "#/definitions/Error",
            },
          },
        },
        // deprecated: true,
        // schemes: ["http", "https", "ws", "wss"],
        // security: [{apiKey: []}, {basic: []}],
      },
    },
    "/jobs/{actionNamespace}/{actionName}/latest": {
      get: {
        tags: ["job"],
        summary: "Get latest job of an existing action",
        // description: "",
        // externalDocs: {},
        operationId: "jobs.get",
        // consumes: ["application/json"],
        // produces: ["application/json"],
        parameters: [
          {
            $ref: "#/parameters/actionNamespaceParam",
          },
          {
            $ref: "#/parameters/actionNameParam",
          },
          {
            $ref: "#/parameters/showParam",
          },
        ],
        responses: {
          "200": {
            description: "A wrapper containing the requested job",
            schema: {
              type: "object",
              properties: {
                apiVersion: {
                  type: "string",
                },
                data: {
                  $ref: "#/definitions/Job",
                },
              },
              required: [
                "apiVersion",
                "data",
              ],
            },
          },
          default: {
            description: "Error payload",
            schema: {
              $ref: "#/definitions/Error",
            },
          },
        },
        // deprecated: true,
        // schemes: ["http", "https", "ws", "wss"],
        // security: {},
      },
    },
    "/jobs/{actionNamespace}/{actionName}/{createdAt}": {
      delete: {
        tags: ["job"],
        summary: "Delete an existing job",
        // description: "",
        // externalDocs: {},
        operationId: "jobs.del",
        // consumes: ["application/json"],
        // produces: ["application/json"],
        parameters: [
          {
            $ref: "#/parameters/actionNamespaceParam",
          },
          {
            $ref: "#/parameters/actionNameParam",
          },
          {
            $ref: "#/parameters/createdAtParam",
          },
          {
            $ref: "#/parameters/apiKeyRequiredParam",
          },
        ],
        responses: {
          "200": {
            description: "A wrapper containing the deleted job",
            schema: {
              type: "object",
              properties: {
                apiVersion: {
                  type: "string",
                },
                data: {
                  $ref: "#/definitions/Job",
                },
              },
              required: [
                "apiVersion",
                "data",
              ],
            },
          },
          default: {
            description: "Error payload",
            schema: {
              $ref: "#/definitions/Error",
            },
          },
        },
        // deprecated: true,
        // schemes: ["http", "https", "ws", "wss"],
        // security: [{apiKey: []}, {basic: []}],
      },
      get: {
        tags: ["job"],
        summary: "Get an existing job",
        // description: "",
        // externalDocs: {},
        operationId: "jobs.get",
        // consumes: ["application/json"],
        // produces: ["application/json"],
        parameters: [
          {
            $ref: "#/parameters/actionNamespaceParam",
          },
          {
            $ref: "#/parameters/actionNameParam",
          },
          {
            $ref: "#/parameters/createdAtParam",
          },
          {
            $ref: "#/parameters/showParam",
          },
        ],
        responses: {
          "200": {
            description: "A wrapper containing the requested job",
            schema: {
              type: "object",
              properties: {
                apiVersion: {
                  type: "string",
                },
                data: {
                  $ref: "#/definitions/Job",
                },
              },
              required: [
                "apiVersion",
                "data",
              ],
            },
          },
          default: {
            description: "Error payload",
            schema: {
              $ref: "#/definitions/Error",
            },
          },
        },
        // deprecated: true,
        // schemes: ["http", "https", "ws", "wss"],
        // security: {},
      },
    },
    "/jobs/{actionNamespace}/{actionName}/latest/redo": {
      post: {
        tags: ["job"],
        summary: "Redo latest job of existing action",
        // description: "",
        // externalDocs: {},
        operationId: "jobs.redo",
        // consumes: ["application/json"],
        // produces: ["application/json"],
        parameters: [
          {
            $ref: "#/parameters/actionNamespaceParam",
          },
          {
            $ref: "#/parameters/actionNameParam",
          },
          {
            $ref: "#/parameters/apiKeyRequiredParam",
          },
        ],
        responses: {
          "201": {
            description: "A wrapper containing the created job",
            schema: {
              type: "object",
              properties: {
                apiVersion: {
                  type: "string",
                },
                data: {
                  $ref: "#/definitions/Job",
                },
              },
              required: [
                "apiVersion",
                "data",
              ],
            },
          },
          default: {
            description: "Error payload",
            schema: {
              $ref: "#/definitions/Error",
            },
          },
        },
        // deprecated: true,
        // schemes: ["http", "https", "ws", "wss"],
        // security: [{apiKey: []}, {basic: []}],
      },
    },
    "/jobs/{actionNamespace}/{actionName}/{createdAt}/redo": {
      post: {
        tags: ["job"],
        summary: "Redo an existing job",
        // description: "",
        // externalDocs: {},
        operationId: "jobs.redo",
        // consumes: ["application/json"],
        // produces: ["application/json"],
        parameters: [
          {
            $ref: "#/parameters/actionNamespaceParam",
          },
          {
            $ref: "#/parameters/actionNameParam",
          },
          {
            $ref: "#/parameters/createdAtParam",
          },
          {
            $ref: "#/parameters/apiKeyRequiredParam",
          },
        ],
        responses: {
          "201": {
            description: "A wrapper containing the created job",
            schema: {
              type: "object",
              properties: {
                apiVersion: {
                  type: "string",
                },
                data: {
                  $ref: "#/definitions/Job",
                },
              },
              required: [
                "apiVersion",
                "data",
              ],
            },
          },
          default: {
            description: "Error payload",
            schema: {
              $ref: "#/definitions/Error",
            },
          },
        },
        // deprecated: true,
        // schemes: ["http", "https", "ws", "wss"],
        // security: [{apiKey: []}, {basic: []}],
      },
    },
    "/login": {
      post: {
        tags: ["user"],
        summary: "Login (to retrieve API key)",
        // description: "",
        // externalDocs: {},
        operationId: "users.login",
        // consumes: ["application/json"],
        // produces: ["application/json"],
        parameters: [
          {
            name: "user",
            in: "body",
            // description: "",
            required: true,
            schema: {
              type: "object",
              properties: {
                password: {
                  type: "string",
                },
                userName: {
                  type: "string",
                },
              },
              required: [
                "password",
                "userName",
              ],
            },
          },
        ],
        responses: {
          "200": {
            description: "A wrapper containing the logged-in user (with its API key)",
            schema: {
              type: "object",
              properties: {
                apiVersion: {
                  type: "string",
                },
                data: {
                  $ref: "#/definitions/User",
                },
              },
              required: [
                "apiVersion",
                "data",
              ],
            },
          },
          default: {
            description: "Error payload",
            schema: {
              $ref: "#/definitions/Error",
            },
          },
        },
        // deprecated: true,
        // schemes: ["http", "https", "ws", "wss"],
        // security: {},
      },
    },
    "/organizations": {
      get: {
        tags: ["organization"],
        summary: "List IDs of organizations",
        // description: "",
        // externalDocs: {},
        operationId: "organizations.list",
        // consumes: ["application/json"],
        // produces: ["application/json"],
        parameters: [],
        responses: {
          "200": {
            description: "A wrapper containing references to organizations",
            schema: {
              type: "object",
              properties: {
                apiVersion: {
                  type: "string",
                },
                data: {
                  type: "array",
                  items: {
                    $ref: "#/definitions/UrlName",
                  },
                },
              },
              required: [
                "apiVersion",
                "data",
              ],
            },
          },
          default: {
            description: "Error payload",
            schema: {
              $ref: "#/definitions/Error",
            },
          },
        },
        // deprecated: true,
        // schemes: ["http", "https", "ws", "wss"],
        // security: {},
      },
      post: {
        tags: ["organization"],
        summary: "Create a new organization",
        // description: "",
        // externalDocs: {},
        operationId: "organizations.create",
        // consumes: ["application/json"],
        // produces: ["application/json"],
        parameters: [
          {
            $ref: "#/parameters/organizationParam",
          },
          {
            $ref: "#/parameters/apiKeyRequiredParam",
          },
        ],
        responses: {
          "201": {
            description: "A wrapper containing the created organization",
            schema: {
              type: "object",
              properties: {
                apiVersion: {
                  type: "string",
                },
                data: {
                  $ref: "#/definitions/Organization",
                },
              },
              required: [
                "apiVersion",
                "data",
              ],
            },
          },
          default: {
            description: "Error payload",
            schema: {
              $ref: "#/definitions/Error",
            },
          },
        },
        // deprecated: true,
        // schemes: ["http", "https", "ws", "wss"],
        // security: {},
      },
    },
    "/organizations/{organizationName}": {
      delete: {
        tags: ["organization"],
        summary: "Delete an existing organization",
        // description: "",
        // externalDocs: {},
        operationId: "organizations.del",
        // consumes: ["application/json"],
        // produces: ["application/json"],
        parameters: [
          {
            $ref: "#/parameters/organizationNameParam",
          },
          {
            $ref: "#/parameters/apiKeyRequiredParam",
          },
        ],
        responses: {
          "200": {
            description: "A wrapper containing the deleted organization",
            schema: {
              type: "object",
              properties: {
                apiVersion: {
                  type: "string",
                },
                data: {
                  $ref: "#/definitions/Organization",
                },
              },
              required: [
                "apiVersion",
                "data",
              ],
            },
          },
          default: {
            description: "Error payload",
            schema: {
              $ref: "#/definitions/Error",
            },
          },
        },
        // deprecated: true,
        // schemes: ["http", "https", "ws", "wss"],
        // security: [{apiKey: []}, {basic: []}],
      },
      get: {
        tags: ["organization"],
        summary: "Get an organization",
        // description: "",
        // externalDocs: {},
        operationId: "organizations.get",
        // consumes: ["application/json"],
        // produces: ["application/json"],
        parameters: [
          {
            $ref: "#/parameters/organizationNameParam",
          },
          {
            $ref: "#/parameters/showParam",
          },
        ],
        responses: {
          "200": {
            description: "A wrapper containing the requested organization",
            schema: {
              type: "object",
              properties: {
                apiVersion: {
                  type: "string",
                },
                data: {
                  $ref: "#/definitions/Organization",
                },
              },
              required: [
                "apiVersion",
                "data",
              ],
            },
          },
          default: {
            description: "Error payload",
            schema: {
              $ref: "#/definitions/Error",
            },
          },
        },
        // deprecated: true,
        // schemes: ["http", "https", "ws", "wss"],
        // security: {},
      },
    },
    "/users": {
      get: {
        tags: ["user"],
        summary: "List IDs of users",
        // description: "",
        // externalDocs: {},
        operationId: "users.list",
        // consumes: ["application/json"],
        // produces: ["application/json"],
        parameters: [],
        responses: {
          "200": {
            description: "A wrapper containing references to users",
            schema: {
              type: "object",
              properties: {
                apiVersion: {
                  type: "string",
                },
                data: {
                  type: "array",
                  items: {
                    $ref: "#/definitions/UrlName",
                  },
                },
              },
              required: [
                "apiVersion",
                "data",
              ],
            },
          },
          default: {
            description: "Error payload",
            schema: {
              $ref: "#/definitions/Error",
            },
          },
        },
        // deprecated: true,
        // schemes: ["http", "https", "ws", "wss"],
        // security: {},
      },
      post: {
        tags: ["user"],
        summary: "Create a new user",
        // description: "",
        // externalDocs: {},
        operationId: "users.create",
        // consumes: ["application/json"],
        // produces: ["application/json"],
        parameters: [
          {
            $ref: "#/parameters/userParam",
          },
        ],
        responses: {
          "201": {
            description: "A wrapper containing the created user",
            schema: {
              type: "object",
              properties: {
                apiVersion: {
                  type: "string",
                },
                data: {
                  $ref: "#/definitions/User",
                },
              },
              required: [
                "apiVersion",
                "data",
              ],
            },
          },
          default: {
            description: "Error payload",
            schema: {
              $ref: "#/definitions/Error",
            },
          },
        },
        // deprecated: true,
        // schemes: ["http", "https", "ws", "wss"],
        // security: {},
      },
      // put: {
      //   tags: ["user"],
      //   summary: "Update an existing user",
      //   // description: "",
      //   // externalDocs: {},
      //   operationId: "users.update",
      //   // consumes: ["application/json"],
      //   // produces: ["application/json"],
      //   parameters: [
      //     {
      //       $ref: "#/parameters/userParam",
      //     },
      //     {
      //       $ref: "#/parameters/apiKeyRequiredParam",
      //     },
      //   ],
      //   responses: {
      //     "200": {
      //       description: "A wrapper containing the updated user",
      //       schema: {
      //         type: "object",
      //         properties: {
      //           apiVersion: {
      //             type: "string",
      //           },
      //           data: {
      //             $ref: "#/definitions/User",
      //           },
      //         },
      //         required: [
      //           "apiVersion",
      //           "data",
      //         ],
      //       },
      //     },
      //     default: {
      //       description: "Error payload",
      //       schema: {
      //         $ref: "#/definitions/Error",
      //       },
      //     },
      //   },
      //   deprecated: true,
      //   schemes: ["http", "https", "ws", "wss"],
      //   security: [{apiKey: []}, {basic: []}],
      // },
    },
    "/users/{userName}": {
      delete: {
        tags: ["user"],
        summary: "Delete an existing user",
        // description: "",
        // externalDocs: {},
        operationId: "users.del",
        // consumes: ["application/json"],
        // produces: ["application/json"],
        parameters: [
          {
            $ref: "#/parameters/userNameParam",
          },
          {
            $ref: "#/parameters/apiKeyRequiredParam",
          },
        ],
        responses: {
          "200": {
            description: "A wrapper containing the deleted user",
            schema: {
              type: "object",
              properties: {
                apiVersion: {
                  type: "string",
                },
                data: {
                  $ref: "#/definitions/User",
                },
              },
              required: [
                "apiVersion",
                "data",
              ],
            },
          },
          default: {
            description: "Error payload",
            schema: {
              $ref: "#/definitions/Error",
            },
          },
        },
        // deprecated: true,
        // schemes: ["http", "https", "ws", "wss"],
        // security: [{apiKey: []}, {basic: []}],
      },
      get: {
        tags: ["user"],
        summary: "Get a user",
        // description: "",
        // externalDocs: {},
        operationId: "users.get",
        // consumes: ["application/json"],
        // produces: ["application/json"],
        parameters: [
          {
            $ref: "#/parameters/userNameParam",
          },
          {
            $ref: "#/parameters/showParam",
          },
        ],
        responses: {
          "200": {
            description: "A wrapper containing the requested user",
            schema: {
              type: "object",
              properties: {
                apiVersion: {
                  type: "string",
                },
                data: {
                  $ref: "#/definitions/User",
                },
              },
              required: [
                "apiVersion",
                "data",
              ],
            },
          },
          default: {
            description: "Error payload",
            schema: {
              $ref: "#/definitions/Error",
            },
          },
        },
        // deprecated: true,
        // schemes: ["http", "https", "ws", "wss"],
        // security: {},
      },
    },
    // parameters: {},
  },
  definitions: {
    Action: {
      type: "object",
      discriminator: "kind",
      properties: {
        createdAt: {
          type: "string",
          format: "date-time",
        },
        description: {
          type: "string",
        },
        key: {
          type: "string",
        },
        kind: {
          type: "string",
          enum: [
            "SecureShellScriptAction",
            "ShellScriptAction",
          ],
        },
        mergePending: {
          type: "boolean",
        },
        name: {
          type: "string",
        },
        private: {
          type: "object",
          additionalProperties: {
            type: "string",
          },
        },
        urlName: {
          $ref: "#/definitions/UrlName",
        },
        urlNamespace: {
          $ref: "#/definitions/UrlName",
        },
      },
      required: [
        "kind",
        "urlName",
        "urlNamespace",
      ],
    },
    ActionReference: {
      type: "object",
      properties: {
        urlName: {
          $ref: "#/definitions/UrlName",
        },
        urlNamespace: {
          $ref: "#/definitions/UrlName",
        },
      },
      required: [
        "urlName",
        "urlNamespace",
      ],
    },
    Dataset: {
      type: "object",
      properties: {
        createdAt: {
          type: "string",
          format: "date-time",
        },
        description: {
          type: "string",
        },
        eventKey: {
          type: "string",
        },
        name: {
          type: "string",
        },
        url: {
          type: "string",
          format: "url",
        },
        urlName: {
          $ref: "#/definitions/UrlName",
        },
        urlNamespace: {
          $ref: "#/definitions/UrlName",
        },
      },
      required: [
        "urlName",
        "urlNamespace",
      ],
    },
    DatasetReference: {
      type: "object",
      properties: {
        urlName: {
          $ref: "#/definitions/UrlName",
        },
        urlNamespace: {
          $ref: "#/definitions/UrlName",
        },
      },
      required: [
        "urlName",
        "urlNamespace",
      ],
    },
    Error: {
      type: "object",
      properties: {
        apiVersion: {
          type: "string",
        },
        code: {
          type: "integer",
          minimum: 100,
          maximum: 600,
        },
        message: {
          type: "string",
        },
      },
      required: [
        "apiVersion",
        "code",
        "message",
      ],
    },
    Event: {
      type: "object",
      properties: {
        body: {
          type: "string",
        },
        createdAt: {
          type: "string",
          format: "date-time",
        },
        headers: {
          type: "object",
          additionalProperties: {
            type: "string",
          },
        },
        id: {
          $ref: "#/definitions/Id",
        },
        method: {
          type: "string",
        },
        querystring: {
          type: "string",
        },
        remoteAddress: {
          type: "string",
        },
        remotePort: {
          type: "integer",
        },
        status: {
          type: "string",
          enum: [
            "processed",
            "processing",
            // null or undefined for "pending"
          ],
        },
        type: {
          $ref: "#/definitions/EventTypeReference",
        },
      },
      required: [
        "type",
      ],
    },
    EventReference: {
      type: "object",
      properties: {
        createdAt: {
          type: "string",
          format: "date-time",
        },
        type: {
          $ref: "#/definitions/EventTypeReference",
        },
      },
      required: [
        "createdAt",
        "type",
      ],
    },
    EventType: {
      type: "object",
      properties: {
        createdAt: {
          type: "string",
          format: "date-time",
        },
        dataset: {
          $ref: "#/definitions/DatasetReference",
        },
        id: {
          $ref: "#/definitions/Id",
        },
        urlName: {
          type: "string",
        },
      },
      required: [
        "dataset",
        "urlName",
      ],
    },
    EventTypeReference: {
      type: "object",
      properties: {
        dataset: {
          $ref: "#/definitions/DatasetReference",
        },
        urlName: {
          $ref: "#/definitions/UrlName",
        },
      },
      required: [
        "dataset",
        "urlName",
      ],
    },
    Id: {
      type: "string",
      pattern: "^[0-9a-f]{8}-[0-9a-f]{4}-[0-9a-f]{4}-[0-9a-f]{4}-[0-9a-f]{8}$",
    },
    Job: {
      type: "object",
      properties: {
        action: {
          $ref: "#/definitions/ActionReference",
        },
        commands: {
          type: "array",
          items: {
            type: "object",
            properties: {
              stderr: {
                type: "string",
              },
              stdout: {
                type: "string",
              },
            },
          },
        },
        createdAt: {
          type: "string",
          format: "date-time",
        },
        event: {
          $ref: "#/definitions/EventReference",
        },
        exitCode: {
          type: "integer",
        },
        id: {
          $ref: "#/definitions/Id",
        },
        request: {
          type: "object",
          properties: {
            body: {
              type: "string",
            },
            headers: {
              type: "object",
              additionalProperties: {
                type: "string",
              },
            },
            method: {
              type: "string",
            },
            querystring: {
              type: "string",
            },
            remoteAddress: {
              type: "string",
            },
            remotePort: {
              type: "integer",
            },
          },
        },
        script: {
          type: "string",
        },
        signal: {
          type: "string",
        },
        status: {
          type: "string",
          enum: [
            "processed",
            "processing",
            // null or undefined for "pending"
          ],
        },
      },
      required: [
        "action",
      ],
    },
    JobReference: {
      type: "object",
      properties: {
        createdAt: {
          type: "string",
          format: "date-time",
        },
        urlName: {
          $ref: "#/definitions/UrlName",
        },
        urlNamespace: {
          $ref: "#/definitions/UrlName",
        },
      },
      required: [
        "createdAt",
        "urlName",
        "urlNamespace",
      ],
    },
    Namespace: {
      type: "object",
      properties: {
        id: {
          $ref: "#/definitions/Id",
        },
        kind: {
          type: "string",
          enum: [
            "organization",
            "user",
          ],
        },
        // organization: {
        //   $ref: "#/definitions/OrganizationSummary",
        // },
        urlName: {
          type: "string",
        },
        // user: {
        //   $ref: "#/definitions/UserSummary",
        // },
      },
      required: [
        "id",
        "kind",
        "urlName",
      ],
    },
    Organization: {
      type: "object",
      properties: {
        id: {
          $ref: "#/definitions/Id",
        },
        name: {
          type: "string",
        },
        members: {
          type: "array",
          items: {
            type: "string",
          },
        },
        urlName: {
          type: "string",
        },
      },
      required: [
        "urlName",
      ],
    },
    SecureShellScriptAction: {
      // description: "",
      allOf: [
        {
          $ref: "#/definitions/Action",
        },
        {
          properties: {
            agentForward: {
              type: "boolean",
            },
            host: {
              type: "string",
            },
            publicKey: {
              type: "string",
            },
            script: {
              type: "string",
            },
            username: {
              type: "string",
            },
          },
          required: [
            "host",
            "publicKey",
            "username",
          ],
        },
      ],
    },
    ShellScriptAction: {
      // description: "",
      allOf: [
        {
          $ref: "#/definitions/Action",
        },
        {
          properties: {
            script: {
              type: "string",
            },
          },
          // required: [],
        },
      ],
    },
    UrlName: {
      type: "string",
    },
    User: {
      type: "object",
      properties: {
        apiKey: {
          type: "string",
        },
        id: {
          $ref: "#/definitions/Id",
        },
        name: {
          type: "string",
        },
        organizations: {
          type: "array",
          items: {
            type: "string",
          },
        },
        password: {
          type: "string",
        },
        urlName: {
          type: "string",
        },
      },
      required: [
        "urlName",
      ],
    },
  },
  parameters: {
    actionKeyOptionalParam: {
      description: "Secret key used by clients to prove that they can handle action",
      // format: "password",  Don't use password format to allow key to be visible in Swagger-UI.
      in: "header",
      name: "databrew-action-key",
      type: "string",
    },
    actionKeyRequiredParam: {
      description: "Secret key used by clients to prove that they can handle action",
      // format: "password",  Don't use password format to allow key to be visible in Swagger-UI.
      in: "header",
      name: "databrew-action-key",
      required: true,
      type: "string",
    },
    actionNameParam: {
      // description: "",
      in: "path",
      name: "actionName",
      required: true,
      type: "string",
    },
    actionNamespaceParam: {
      // description: "",
      in: "path",
      name: "actionNamespace",
      required: true,
      type: "string",
    },
    actionParam: {
      // description: "",
      in: "body",
      name: "action",
      required: true,
      schema: {
        $ref: "#/definitions/Action",
      },
    },
    apiKeyOptionalParam: {
      description: "Secret key used to identify user",
      // format: "password",  Don't use password format to allow key to be visible in Swagger-UI.
      in: "header",
      name: "databrew-api-key",
      type: "string",
    },
    apiKeyRequiredParam: {
      description: "Secret key used to identify user",
      // format: "password",  Don't use password format to allow key to be visible in Swagger-UI.
      in: "header",
      name: "databrew-api-key",
      required: true,
      type: "string",
    },
    createdAtParam: {
      // description: "",
      in: "path",
      name: "createdAt",
      required: true,
      type: "string",
      format: "date-time",
    },
    datasetNameParam: {
      // description: "",
      in: "path",
      name: "datasetName",
      required: true,
      type: "string",
    },
    datasetNamespaceParam: {
      // description: "",
      in: "path",
      name: "datasetNamespace",
      required: true,
      type: "string",
    },
    datasetParam: {
      // description: "",
      in: "body",
      name: "dataset",
      required: true,
      schema: {
        $ref: "#/definitions/Dataset",
      },
    },
    eventKeyHeaderParam: {
      description: "Secret key used by clients to prove that they are allowed to send an event to dataset",
      // format: "password",  Don't use password format to allow key to be visible in Swagger-UI.
      in: "header",
      name: "databrew-event-key",
      required: false,
      type: "string",
    },
    eventKeyQueryParam: {
      description: "Secret key used by clients to prove that they are allowed to send an event to dataset",
      // format: "password",  Don't use password format to allow key to be visible in Swagger-UI.
      in: "query",
      name: "eventKey",
      required: false,
      type: "string",
    },
    eventParam: {
      // description: "",
      in: "body",
      name: "event",
      required: true,
      schema: {
        $ref: "#/definitions/Event",
      },
    },
    eventTypeNameParam: {
      // description: "",
      in: "path",
      name: "eventTypeName",
      required: true,
      type: "string",
    },
    idParam: {
      // description: "",
      in: "path",
      name: "id",
      required: true,
      // A reference to a non-object definition doesn't work for a parameter that is not in request body.
      // schema: {
      //   $ref: "#/definitions/Id",
      // },
      type: "string",
      pattern: "^[0-9a-f]{8}-[0-9a-f]{4}-[0-9a-f]{4}-[0-9a-f]{4}-[0-9a-f]{8}$",
    },
    jobKeyOptionalParam: {
      description: "Secret key used by an action (its script...) to identify the current job",
      // format: "password",  Don't use password format to allow key to be visible in Swagger-UI.
      in: "header",
      name: "databrew-job-key",
      type: "string",
    },
    jobKeyRequiredParam: {
      description: "Secret key used by an action (its script...) to identify the current job",
      // format: "password",  Don't use password format to allow key to be visible in Swagger-UI.
      in: "header",
      name: "databrew-job-key",
      required: true,
      type: "string",
    },
    organizationNameParam: {
      // description: "",
      in: "path",
      name: "organizationName",
      required: true,
      type: "string",
    },
    organizationParam: {
      // description: "",
      in: "body",
      name: "organization",
      required: true,
      schema: {
        $ref: "#/definitions/Organization",
      },
    },
    showParam: {
      // description: "",
      in: "query",
      name: "show",
      type: "array",
      items: {
        type: "string",
      },
      collectionFormat: "multi",
    },
    userNameParam: {
      // description: "",
      in: "path",
      name: "userName",
      required: true,
      type: "string",
    },
    userParam: {
      // description: "",
      in: "body",
      name: "user",
      required: true,
      schema: {
        $ref: "#/definitions/User",
      },
    },
  },
  // externalDocs: {},
  // responses: {},
  // security: {},
  // securityDefinitions: {
  //   apiKey: {
  //     description: "Secret key used to identify user or bot",
  //     in: "header",
  //     name: "databrew-api-key",
  //     type: "apiKey",
  //   },
  //   basic: {
  //     description: "HTTP Basic authentication with user (or bot) name and password",
  //     type: "basic",
  //   },
  // },
  // tags: [],
}


export {get}
async function get(ctx){
  ctx.body = JSON.stringify(SPEC)
}
