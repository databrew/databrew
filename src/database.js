// Databrew -- A data flow brewer, logger & dashboard
// By: Emmanuel Raviart <emmanuel@raviart.com>
//
// Copyright (C) 2015 Emmanuel Raviart
// https://git.framasoft.org/databrew/databrew
//
// Databrew is free software; you can redistribute it and/or modify
// it under the terms of the GNU Affero General Public License as
// published by the Free Software Foundation, either version 3 of the
// License, or (at your option) any later version.
//
// Databrew is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Affero General Public License for more details.
//
// You should have received a copy of the GNU Affero General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.


import assert from "assert"
import {randomBytes} from "mz/crypto"
import rethinkdbdashFactory from "rethinkdbdash"

import config from "./config"


export const r = rethinkdbdashFactory({
    db: config.db.name,
    host: config.db.host,
    port: config.db.port,
  })

const versionNumber = 1


export {checkDatabase}
async function checkDatabase() {
  const databasesName = await r.dbList()
  assert(databasesName.includes(config.db.name), `Database is not initialized. Run "node configure" to configure it.`)
  let versions
  try {
    versions = await r.table("version")
  } catch (e) {
    throw new Error(`Database is not initialized. Run "node configure" to configure it.`)
  }
  assert(versions.length > 0, `Database is not initialized. Run "node configure" to configure it.`)
  assert.strictEqual(versions.length, 1)
  const version = versions[0]
  assert(version.number <= versionNumber, `Database format is too recent. Upgrade Databrew.`)
  assert.strictEqual(version.number, versionNumber, `Database must be upgraded. Run "node configure" to configure it.`)
}


export {configure}
async function configure() {
  const databasesName = await r.dbList()
  if (!databasesName.includes(config.db.name)) {
    await r.dbCreate(config.db.name)
    await r.tableCreate("version")
    await r.table("version").insert({number: versionNumber})
  }

  try {
    await r.table("actions").count()
  } catch (e) {
    await r.tableCreate("actions")
  }
  const actionsTable = r.table("actions")
  try {
    await actionsTable.indexWait("createdAt")
  } catch (e) {
    await actionsTable.indexCreate("createdAt")
  }
  try {
    await actionsTable.indexWait("namespaceId")
  } catch (e) {
    await actionsTable.indexCreate("namespaceId")
  }
  try {
    await actionsTable.indexWait("namespaceIdAndUrlName")
  } catch (e) {
    await actionsTable.indexCreate("namespaceIdAndUrlName", [
      r.row("namespaceId"),
      r.row("urlName"),
    ])
  }
  try {
    await actionsTable.indexWait("sourcesId")
  } catch (e) {
    await actionsTable.indexCreate("sourcesId", {multi: true})
  }

  try {
    await r.table("datasets").count()
  } catch (e) {
    await r.tableCreate("datasets")
  }
  const datasetsTable = r.table("datasets")
  try {
    await datasetsTable.indexWait("createdAt")
  } catch (e) {
    await datasetsTable.indexCreate("createdAt")
  }
  try {
    await datasetsTable.indexWait("namespaceId")
  } catch (e) {
    await datasetsTable.indexCreate("namespaceId")
  }
  try {
    await datasetsTable.indexWait("namespaceIdAndUrlName")
  } catch (e) {
    await datasetsTable.indexCreate("namespaceIdAndUrlName", [
      r.row("namespaceId"),
      r.row("urlName"),
    ])
  }

  try {
    await r.table("events").count()
  } catch (e) {
    await r.tableCreate("events")
  }
  const eventsTable = r.table("events")
  try {
    await eventsTable.indexWait("createdAt")
  } catch (e) {
    await eventsTable.indexCreate("createdAt")
  }
  try {
    await eventsTable.indexWait("pendingCreatedAt")
  } catch (e) {
    await eventsTable.indexCreate("pendingCreatedAt", function (event) {
      return r.branch(
        event.hasFields("status"),
        r.error("A non-pending event is not indexed."),
        event("createdAt"),
      )
    })
  }
  try {
    await eventsTable.indexWait("typeId")
  } catch (e) {
    await eventsTable.indexCreate("typeId")
  }
  try {
    await eventsTable.indexWait("typeIdAndCreatedAt")
  } catch (e) {
    await eventsTable.indexCreate("typeIdAndCreatedAt", [
      r.row("typeId"),
      r.row("createdAt"),
    ])
  }

  try {
    await r.table("eventTypes").count()
  } catch (e) {
    await r.tableCreate("eventTypes")
  }
  const eventTypesTable = r.table("eventTypes")
  try {
    await eventTypesTable.indexWait("datasetId")
  } catch (e) {
    await eventTypesTable.indexCreate("datasetId")
  }
  try {
    await eventTypesTable.indexWait("datasetIdAndUrlName")
  } catch (e) {
    await eventTypesTable.indexCreate("datasetIdAndUrlName", [
      r.row("datasetId"),
      r.row("urlName"),
    ])
  }

  try {
    await r.table("jobs").count()
  } catch (e) {
    await r.tableCreate("jobs")
  }
  const jobsTable = r.table("jobs")
  try {
    await jobsTable.indexWait("actionId")
  } catch (e) {
    await jobsTable.indexCreate("actionId")
  }
  try {
    await jobsTable.indexWait("actionIdAndCreatedAt")
  } catch (e) {
    await jobsTable.indexCreate("actionIdAndCreatedAt", [
      r.row("actionId"),
      r.row("createdAt"),
    ])
  }
  try {
    await jobsTable.indexWait("createdAt")
  } catch (e) {
    await jobsTable.indexCreate("createdAt")
  }
  try {
    await jobsTable.indexWait("eventId")
  } catch (e) {
    await jobsTable.indexCreate("eventId")
  }
  try {
    await jobsTable.indexWait("key")
  } catch (e) {
    await jobsTable.indexCreate("key")
  }
  try {
    await jobsTable.indexWait("pendingActionId")
  } catch (e) {
    await jobsTable.indexCreate("pendingActionId", function (job) {
      return r.branch(
        job.hasFields("status"),
        r.error("A non-pending job is not indexed."),
        job("actionId"),
      )
    })
  }
  try {
    await jobsTable.indexWait("pendingCreatedAt")
  } catch (e) {
    await jobsTable.indexCreate("pendingCreatedAt", function (job) {
      return r.branch(
        job.hasFields("status"),
        r.error("A non-pending job is not indexed."),
        job("createdAt"),
      )
    })
  }

  try {
    await r.table("namespaces").count()
  } catch (e) {
    await r.tableCreate("namespaces")
  }
  const namespacesTable = r.table("namespaces")
  try {
    await namespacesTable.indexWait("urlName")
  } catch (e) {
    await namespacesTable.indexCreate("urlName")
  }

  try {
    await r.table("organizations").count()
  } catch (e) {
    await r.tableCreate("organizations")
  }
  const organizationsTable = r.table("organizations")
  try {
    await organizationsTable.indexWait("createdAt")
  } catch (e) {
    await organizationsTable.indexCreate("createdAt")
  }
  try {
    await organizationsTable.indexWait("urlName")
  } catch (e) {
    await organizationsTable.indexCreate("urlName")
  }

  try {
    await r.table("users").count()
  } catch (e) {
    await r.tableCreate("users")
  }
  const usersTable = r.table("users")
  try {
    await usersTable.indexWait("apiKey")
  } catch (e) {
    await usersTable.indexCreate("apiKey")
  }
  try {
    await usersTable.indexWait("createdAt")
  } catch (e) {
    await usersTable.indexCreate("createdAt")
  }
  try {
    await usersTable.indexWait("organizationsId")
  } catch (e) {
    await usersTable.indexCreate("organizationsId", {multi: true})
  }
  try {
    await usersTable.indexWait("urlName")
  } catch (e) {
    await usersTable.indexCreate("urlName")
  }

  try {
    await r.table("version").count()
  } catch (e) {
    await r.tableCreate("version")
  }
  const versionTable = r.table("version")
  const versions = await versionTable
  let version
  if (versions.length === 0) {
    let result = await versionTable.insert({number: 0}, {returnChanges: true})
    version = result.changes[0].new_val
  } else {
    assert.strictEqual(versions.length, 1)
    version = versions[0]
  }
  assert(version.number <= versionNumber, `Database format is too recent. Upgrade Databrew.`)

  const previousVersionNumber = version.number

  if (version.number === 0) {
    // Add key to actions.
    let actionsTable = r.table("actions")
    let actionsId = await actionsTable
      .filter(r.row.hasFields("key").not())
      .getField("id")
    for (let actionId of actionsId) {
      await actionsTable
        .get(actionId)
        .update({
          key: (await randomBytes(16)).toString("hex"),  // 128 bits
        })
    }

    version.number += 1
  }

  assert(version.number <= versionNumber,
    `Error in database upgrade script: Wrong version number: ${version.number} > ${versionNumber}.`)
  assert(version.number >= previousVersionNumber,
    `Error in database upgrade script: Wrong version number: ${version.number} < ${previousVersionNumber}.`)
  if (version.number !== previousVersionNumber) await versionTable.get(version.id).update({number: version.number})
}
