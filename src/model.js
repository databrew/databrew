// Databrew -- A data flow brewer, logger & dashboard
// By: Emmanuel Raviart <emmanuel@raviart.com>
//
// Copyright (C) 2015 Emmanuel Raviart
// https://git.framasoft.org/databrew/databrew
//
// Databrew is free software; you can redistribute it and/or modify
// it under the terms of the GNU Affero General Public License as
// published by the Free Software Foundation, either version 3 of the
// License, or (at your option) any later version.
//
// Databrew is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Affero General Public License for more details.
//
// You should have received a copy of the GNU Affero General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.


import assert from "assert"

import {r} from "./database"


export function* ownsNamespace(ctx, user, namespace) {
  if (!user) return false
  if (user.isAdmin) return true
  switch (namespace.kind) {
    case "organization":
      let organizations = yield r
        .table("organizations")
        .getAll(namespace.urlName, {index: "urlName"})
        .limit(1)
      assert.equal(organizations.length, 1)
      let organization = organizations[0]
      return (user.organizationsId || []).includes(organization.id)
    case "user":
      return user.urlName === namespace.urlName
    default:
      throw new Error(`Unexpected kind for namespace: ${namespace.kind}.`)
  }
}


export function ownsOrganization(user, organization) {
  if (!user) return false
  if (user.isAdmin) return true
  return (user.organizationsId || []).includes(organization.id)
}


export function ownsUser(user, otherUser) {
  if (!user) return false
  if (user.isAdmin) return true
  return user.id === otherUser.id
}
