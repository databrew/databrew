// Databrew -- A data flow brewer, logger & dashboard
// By: Emmanuel Raviart <emmanuel@raviart.com>
//
// Copyright (C) 2015 Emmanuel Raviart
// https://git.framasoft.org/databrew/databrew
//
// Databrew is free software; you can redistribute it and/or modify
// it under the terms of the GNU Affero General Public License as
// published by the Free Software Foundation, either version 3 of the
// License, or (at your option) any later version.
//
// Databrew is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Affero General Public License for more details.
//
// You should have received a copy of the GNU Affero General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.


import {randomBytes} from "mz/crypto"

import {checkDatabase, r} from "./database"


async function processAction(event, action) {
  let namespace = await r
    .table("namespaces")
    .get(action.namespaceId)

  console.log(`  Processing action ${namespace.urlName}/${action.urlName}...`)
  if (!action.script || !action.script.trim()) {
    console.log("    Skipping action with empty script.")
    return
  }

  if (action.mergePending) {
    // Don't create a new job when a job for the same action is already pending.
    let pendingJobs = await r
      .table("jobs")
      .getAll(action.id, {index: "pendingActionId"})
      .limit(1)
    if (pendingJobs.length > 0) {
      let job = pendingJobs[0]
      console.log(`    Ignoring event, because job` +
        ` ${namespace.urlName}/${action.urlName}/${job.createdAt.toISOString()} exists and mergePending is set.`)
      return null  // No new job.
    }
  }

  // Create a job for action.
  let job = {
    actionId: action.id,
    commands: [],
    createdAt: r.now(),
    eventId: event.id,
    key: (await randomBytes(16)).toString("hex"),  // 128 bits
    request: {
      body: event.body,
      headers: event.headers,
      method: event.method,
      querystring: event.querystring,
      remoteAddress: event.remoteAddress,
      remotePort: event.remotePort,
    },
  }
  let result = await r
    .table("jobs")
    .insert(job, {returnChanges: true})
  job = await result.changes[0].new_val
  console.log(`    Queueing job ${namespace.urlName}/${action.urlName}/${job.createdAt.toISOString()}.`)
  return job
}


async function processEvent(event) {
  await r
    .table("events")
    .get(event.id)
    .update({status: "processing"})
  let eventType = await r
    .table("eventTypes")
    .get(event.typeId)
  let dataset = await r
    .table("datasets")
    .get(eventType.datasetId)
  let namespace = await r
    .table("namespaces")
    .get(dataset.namespaceId)
  console.log(`Processing event ${namespace.urlName}/${dataset.urlName}/${eventType.urlName}` +
    `/${event.createdAt.toISOString()}...`)

  let actions = await r
    .table("actions")
    .getAll(eventType.id, {index: "sourcesId"})
    .distinct()
    .orderBy("createdAt")
  for (let action of actions) {
    await processAction(event, action)
  }
  await r
    .table("events")
    .get(event.id)
    .update({status: "processed"})
}


async function processEvents () {
  // First, prepare to handle new pending actions.
  // Don't await, because we want to process existing pending actions now.
  processNewEvents().catch(error => console.log(error.stack))

  // Handle existing pending events.
  let events = await r
    .table("events")
    .orderBy({index: r.desc("pendingCreatedAt")})
  for (let event of events) {
    await processEvent(event)
  }
}


async function processNewEvents() {
  // Handle new pending events.
  let cursor = await r
    .table("events")
    .changes()
    .run({cursor: true})
  // Note: Don't use cursor.each(function), to process pending events one at a time.
  while (true) {
    let change = await cursor.next()
    let event = change.new_val
    if (event !== null && ! event.status) {
      await processEvent(event)
    }
  }
}


checkDatabase()
  .then(processEvents)
  .catch(error => console.log(error.stack))
