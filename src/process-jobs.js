// Databrew -- A data flow brewer, logger & dashboard
// By: Emmanuel Raviart <emmanuel@raviart.com>
//
// Copyright (C) 2015 Emmanuel Raviart
// https://git.framasoft.org/databrew/databrew
//
// Databrew is free software; you can redistribute it and/or modify
// it under the terms of the GNU Affero General Public License as
// published by the Free Software Foundation, either version 3 of the
// License, or (at your option) any later version.
//
// Databrew is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Affero General Public License for more details.
//
// You should have received a copy of the GNU Affero General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.


import assert from "assert"
import {exec} from "mz/child_process"
import {spawn} from "child_process"
import {randomBytes as randomBytesSync} from "crypto"
import {randomBytes} from "mz/crypto"
import fs from "mz/fs"
import os from "os"
import path from "path"
import {Client as SshClient} from "ssh2"

import config from "./config"
import {checkDatabase, r} from "./database"
import packageConfig from "../package"


const GATEWAY_INTERFACE = "CGI/1.1"
const host = [80, 443].includes(config.port) ? config.host.toString() : `${config.host}:${config.port}`
const SERVER_PROTOCOL = "HTTP/1.1"
const SERVER_SOFTWARE = `Databrew/${packageConfig.version}`


function appendCommandToJob(job, command) {
  r
    .table("jobs")
    .get(job.id)
    .update({commands: r.row("commands").append(command)})
    // Don't wait for the update.
    .catch(function (error) {
      console.log(error)
    })
}


async function processJob(job) {
  let request = job.request
  let action = await r
    .table("actions")
    .get(job.actionId)
  let namespace = await r
    .table("namespaces")
    .get(action.namespaceId)

  let jobUpdate = {
    script: action.script,
    status: "processing",
  }
  Object.assign(job, jobUpdate)
  await r
    .table("jobs")
    .get(job.id)
    .update(jobUpdate)

  console.log(`Processing job ${namespace.urlName}/${action.urlName}/${job.createdAt.toISOString()}...`)

  const tmpDir = os.tmpdir()
  let variables = {
    ACTION_URL: `http://${host}/actions/${namespace.urlName}/${action.urlName}`,
    GATEWAY_INTERFACE,
    JOB_KEY_HEADER: `Databrew-Job-Key: ${job.key}`,
    PATH_INFO: "",
    QUERY_STRING: request.querystring,
    REMOTE_ADDR: request.remoteAddress,
    REMOTE_PORT: request.remotePort,
    REQUEST_METHOD: request.method,
    SCRIPT_NAME: `/actions/${namespace.urlName}/${action.urlName}/run`,
    SERVER_NAME: config.host.toString(),
    SERVER_PORT: config.port,
    SERVER_PROTOCOL,
    SERVER_SOFTWARE,
  }
  if ("content-length" in request.headers) variables.CONTENT_LENGTH = request.headers["content-length"]
  if ("content-type" in request.headers) variables.CONTENT_TYPE = request.headers["content-type"]
  for (let headerName in request.headers || {}) {
    if (["content-length", "content-type"].includes(headerName) || headerName.startsWith("databrew-")) continue
    variables[`HTTP_${headerName.toUpperCase().replace(/-/g, "_")}`] = request.headers[headerName]
  }
  Object.assign(variables, action.private || {})

  if (action.kind == "SecureShellScriptAction") {
    if (action.agentForward) {
      // Add private key to ssh-agent to allow SSH Agent Forwarding.
      let token = (await randomBytes(16)).toString("hex")  // 128 bits token.
      let sshKeysDir = path.join(tmpDir, `databrew-ssh-${namespace.urlName}-${action.urlName}-${token}`)
      let sshPrivateKeyPath = path.join(sshKeysDir, "id_rsa")
      let sshPublicKeyPath = path.join(sshKeysDir, "id_rsa.pub")
      await fs.mkdir(sshKeysDir, {mode: 384})  // 0600 in octal
      await fs.writeFile(sshPrivateKeyPath, action.privateKey, {mode: 384})  // 0600 in octal
      await fs.writeFile(sshPublicKeyPath, action.publicKey, {mode: 384})  // 0600 in octal
      await exec(`ssh-add -k ${sshPrivateKeyPath}`)

      jobUpdate = await processSecureShellScript(variables, job, action)

      // Remove private key from ssh-agent.
      await exec(`ssh-add -d -k ${sshPrivateKeyPath}`)
      await fs.unlink(sshPrivateKeyPath)
      await fs.unlink(sshPublicKeyPath)
      await fs.rmdir(sshKeysDir)
    } else {
      jobUpdate = await processSecureShellScript(variables, job, action)
    }
  } else {
    assert.equal(action.kind, "ShellScriptAction")
    let token = (await randomBytes(16)).toString("hex")  // 128 bits token.
    let bodyPath = path.join(tmpDir, `databrew-request-body-${namespace.urlName}-${action.urlName}-${token}`)
    await fs.writeFile(bodyPath, request.body || "")
    variables[`REQUEST_BODY_PATH`] = bodyPath
    let scriptPath = path.join(tmpDir, `databrew-script-${namespace.urlName}-${action.urlName}-${token}`)
    await fs.writeFile(scriptPath, job.script, {mode: 502})  // 0766 in octal

    jobUpdate = await processShellScript(scriptPath, variables, job, action, namespace)

    await fs.unlink(bodyPath)
    await fs.unlink(scriptPath)
  }

  jobUpdate.status = "processed"
  await r
    .table("jobs")
    .get(job.id)
    .update(jobUpdate)
}


async function processJobs () {
  // First, prepare to handle new pending actions.
  // Don't await, because we want to process existing pending actions now.
  processNewJobs().catch(error => console.log(error.stack))

  // Handle existing pending jobs.
  let jobs = await r
    .table("jobs")
    .orderBy({index: r.desc("pendingCreatedAt")})
  for (let job of jobs) {
    await processJob(job)
  }
}


async function processNewJobs() {
  // Handle new pending jobs.
  let cursor = await r
    .table("jobs")
    .changes()
    .run({cursor: true})
  // Note: Don't use cursor.each(function), to process pending actions one at a time.
  while (true) {
    let change = await cursor.next()
    let job = change.new_val
    if (job !== null && ! job.status) {
      await processJob(job)
    }
  }
}


function processSecureShellScript(variables, job, action) {
  return new Promise(function (resolve /* reject */) {
      let sshClient = new SshClient()
      let command = {}

      sshClient.on("close", function (hadError) {
        console.log("  close", hadError)
        let jobUpdate = {}
        if (hadError) jobUpdate.exitCode = 1
        if (Object.getOwnPropertyNames(command).length > 0) {
          jobUpdate.commands = r.row("commands").append(command)
        }
        resolve(jobUpdate)
      })
      sshClient.on("continue", function () {
        console.log("  continue")
      })
      sshClient.on("end", function () {
        console.log("  end")
      })
      sshClient.on("error", function (error) {
        // Wrong host address, wrong SSH public key...
        console.log("  error", error)
        let jobUpdate = {}
        let errorString = error.toString()
        if (errorString) jobUpdate.error = errorString
        if (Object.getOwnPropertyNames(command).length > 0) {
          jobUpdate.commands = r.row("commands").append(command)
          command = {}
        }
        if (Object.getOwnPropertyNames(jobUpdate).length > 0) {
          r
            .table("jobs")
            .get(job.id)
            .update(jobUpdate)
            // Don't wait for the update.
            .catch(function (error) {
              console.log(error)
            })
        }
      })
      sshClient.on("exit", function (code, signal) {
        console.log("  exit", code, signal)
      })
      sshClient.on("ready", function () {
        console.log("  ready")
        // Note: ssh protocol doesn't allow to set environment variables (by default).
        // See: https://github.com/mscdex/ssh2/issues/166.
        let token = randomBytesSync(16).toString("hex")  // 128 bits token.
        let script = [
          "REQUEST_BODY_PATH=`mktemp --suffix='-databrew-request-body'`\n",
          `cat <<'${token}'` + " >${REQUEST_BODY_PATH}\n" + `${job.request.body || ""}\n${token}\n`,
        ]
        for (let variableName in variables) script.push(`${variableName}="${variables[variableName]}"\n`)
        script = script.join("") + job.script + [
          "\n",
          "rm ${REQUEST_BODY_PATH}\n",
        ].join("")
        sshClient.exec(script, function (error, stream) {
          console.log("  exec callback")
          if (error) throw error

          stream.on("close", function (code, signal) {
            console.log("  stream close", code, signal)
            let jobUpdate = {}
            if (code !== null && code !== undefined) jobUpdate.exitCode = code
            if (signal !== null && signal !== undefined) jobUpdate.signal = signal
            if (Object.getOwnPropertyNames(command).length > 0) {
              jobUpdate.commands = r.row("commands").append(command)
              command = {}
            }
            if (Object.getOwnPropertyNames(jobUpdate).length > 0) {
              r
                .table("jobs")
                .get(job.id)
                .update(jobUpdate)
                // Don't wait for the update.
                .catch(function (error) {
                  console.log(error)
                })
            }
            console.log("    stream close ending SSH connection")
            sshClient.end()
          })
          stream.on("continue", function () {
            console.log("  stream continue")
          })
          stream.on("data", function (data) {
            data = data.toString()
            console.log("  stream stdout data: ", data)
            if (data.length > 0) {
              let jobUpdate = {}
              if (Object.getOwnPropertyNames(command).length === 0) {
                command.stdout = data
              } else if (command.stdout) {
                command.stdout += data
              } else {
                jobUpdate.commands = r.row("commands").append(command)
                command = {stdout: data}
              }
              if (Object.getOwnPropertyNames(jobUpdate).length > 0) {
                r
                  .table("jobs")
                  .get(job.id)
                  .update(jobUpdate)
                  // Don't wait for the update.
                  .catch(function (error) {
                    console.log(error)
                  })
              }
            }
          })
          stream.stderr.on("close", function (code, signal) {
            console.log("  stream stderr close", code, signal)
            let jobUpdate = {}
            if (code !== null && code !== undefined) jobUpdate.exitCode = code
            if (signal !== null && signal !== undefined) jobUpdate.signal = signal
            if (Object.getOwnPropertyNames(command).length > 0) {
              jobUpdate.commands = r.row("commands").append(command)
            }
            if (Object.getOwnPropertyNames(jobUpdate).length > 0) {
              r
                .table("jobs")
                .get(job.id)
                .update(jobUpdate)
                // Don't wait for the update.
                .catch(function (error) {
                  console.log(error)
                })
            }
          })
          stream.stderr.on("continue", function () {
            console.log("  stream stderr continue")
          })
          stream.stderr.on("data", function (data) {
            data = data.toString()
            console.log("  stream stderr data: ", data)
            if (data.length > 0) {
              if (Object.getOwnPropertyNames(command).length === 0) {
                command.stderr = data
              } else if (command.stderr) {
                command.stderr += data
              } else {
                appendCommandToJob(job, command)
                command = {stderr: data}
              }
            }
          })
        })
      })

      sshClient.connect({
        agent: action.agentForward ? process.env.SSH_AUTH_SOCK: null,
        agentForward: action.agentForward || false,
        host: action.host,
        port: 22,
        username: action.username,
        privateKey: action.privateKey,
      })
  })
}


function processShellScript(scriptPath, variables, job) {
  return new Promise(function (resolve /* reject */) {
      let childProcess = spawn(scriptPath, {
        // cwd:
        // detached: false,
        env: {...process.env, ...variables},
        // gid:
        // stdio: "pipe",
        // uid:
      })
      let command = {}

      childProcess.on("close", function (code, signal) {
        console.log("  close", code, signal)
        let jobUpdate = {}
        if (code !== null) jobUpdate.exitCode = code
        if (signal !== null) jobUpdate.signal = signal
        if (Object.getOwnPropertyNames(command).length > 0) {
          jobUpdate.commands = r.row("commands").append(command)
        }
        resolve(jobUpdate)
      })
      childProcess.on("error", function (error) {
        console.log("  error", error)
        let jobUpdate = {}
        let errorString = error.toString()
        if (errorString) jobUpdate.error = errorString
        if (Object.getOwnPropertyNames(command).length > 0) {
          jobUpdate.commands = r.row("commands").append(command)
          command = {}
        }
        if (Object.getOwnPropertyNames(jobUpdate).length > 0) {
          r
            .table("jobs")
            .get(job.id)
            .update(jobUpdate)
            // Don't wait for the update.
            .catch(function (error) {
              console.log(error)
            })
        }
      })
      childProcess.on("exit", function (code, signal) {
        console.log("  exit", code, signal)
      })
      childProcess.on("disconnect", function () {
        console.log("  disconnect")
        // command.disconnect = true  TODO?
      })
      childProcess.stderr.on("data", function (data) {
        data = data.toString()
        console.log("  stderr data: ", data)
        if (data.length > 0) {
          if (Object.getOwnPropertyNames(command).length === 0) {
            command.stderr = data
          } else if (command.stderr) {
            command.stderr += data
          } else {
            appendCommandToJob(job, command)
            command = {stderr: data}
          }
        }
      })
      childProcess.stderr.on("error", function (error) {
        console.log("  stderr error: ", error)
      })
      childProcess.stdin.on("error", function (error) {
        // ECONNRESET occurs when script exits before having executed every lines that it contains.
        if (error.code != "ECONNRESET") {
          console.log("  stdin error: ", error)
        }
      })
      childProcess.stdout.on("data", function (data) {
        data = data.toString()
        console.log("  stdout data: ", data)
        if (data.length > 0) {
          if (Object.getOwnPropertyNames(command).length === 0) {
            command.stdout = data
          } else if (command.stdout) {
            command.stdout += data
          } else {
            appendCommandToJob(job, command)
            command = {stdout: data}
          }
        }
      })
      childProcess.stdout.on("error", function (error) {
        console.log("  stdout error: ", error)
      })
  })
}


if (!process.env.SSH_AUTH_SOCK) {
  console.log(`SSH-Agent must be running before launching this script. Use a command like` +
    ` "ssh-agent npm run process-jobs".`)
  process.exit(1)
}


checkDatabase()
  .then(processJobs)
  .catch(error => console.log(error.stack))
