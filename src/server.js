// Databrew -- A data flow brewer, logger & dashboard
// By: Emmanuel Raviart <emmanuel@raviart.com>
//
// Copyright (C) 2015 Emmanuel Raviart
// https://git.framasoft.org/databrew/databrew
//
// Databrew is free software; you can redistribute it and/or modify
// it under the terms of the GNU Affero General Public License as
// published by the Free Software Foundation, either version 3 of the
// License, or (at your option) any later version.
//
// Databrew is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Affero General Public License for more details.
//
// You should have received a copy of the GNU Affero General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.


import bodyParserFactory from "koa-bodyparser"
import convert from "koa-convert"
import corsFactory from "koa-cors"
import http from "http"
import Koa from "koa"
import routerFactory from "koa-router"
import swaggerValidatorFactory from "koa-swagger"
import validateFactory from "koa-validate"

import config from "./config"
import {checkDatabase} from "./database"
import * as actionsController from "./controllers/actions"
import * as datasetsController from "./controllers/datasets"
import * as eventsController from "./controllers/events"
import * as eventTypesController from "./controllers/event_types"
import * as jobsController from "./controllers/jobs"
import * as namespacesController from "./controllers/namespaces"
import * as organizationsController from "./controllers/organizations"
import * as swaggerController from "./controllers/swagger"
import * as usersController from "./controllers/users"


checkDatabase()
  .then(startKoa)
  .catch(console.log.bind(console))


let bodyParser = bodyParserFactory()

// Patch Swagger spec, because ko-swagger uses :xxx instead of {xxx} for parameters in path.
// See https://github.com/rricard/koa-swagger/issues/2.
let patchedSwaggerSpec = {...swaggerController.SPEC}
let paths = {}
for (let path in patchedSwaggerSpec.paths) {
  paths[path.replace(/\/\{/g, "/:").replace(/\}/g, "")] = patchedSwaggerSpec.paths[path]
}
patchedSwaggerSpec.paths = paths
let swaggerValidator = convert(swaggerValidatorFactory(patchedSwaggerSpec))

let validate = convert(validateFactory())

let router = routerFactory()

router.get("/actions", swaggerValidator, actionsController.list)
router.post("/actions", bodyParser, swaggerValidator, usersController.authenticate(true), actionsController.create)
// router.put("/actions", bodyParser, swaggerValidator, actionsController.update)
router.get("/actions/:actionNamespace", swaggerValidator, namespacesController.requireNamespace("actionNamespace"),
  actionsController.listNamespace)
router.delete("/actions/:actionNamespace/:actionName", swaggerValidator,
  namespacesController.requireNamespace("actionNamespace"), actionsController.requireAction,
  usersController.authenticate(true), actionsController.del)
router.get("/actions/:actionNamespace/:actionName", swaggerValidator,
  namespacesController.requireNamespace("actionNamespace"), actionsController.requireAction,
  usersController.authenticate(false), actionsController.get)
router.patch("/actions/:actionNamespace/:actionName", bodyParser, swaggerValidator,
  namespacesController.requireNamespace("actionNamespace"), actionsController.requireAction,
  usersController.authenticate(true), actionsController.patch)
router.get("/actions/:actionNamespace/:actionName/script", validate,
  namespacesController.requireNamespace("actionNamespace"), actionsController.requireAction,
  actionsController.getScript)
router.post("/actions/:actionNamespace/:actionName/script", validate,
  namespacesController.requireNamespace("actionNamespace"), actionsController.requireAction,
  usersController.authenticate(true), actionsController.setScript)
router.get("/actions/:actionNamespace/:actionName/sources", swaggerValidator,
  namespacesController.requireNamespace("actionNamespace"), actionsController.requireAction,
  actionsController.listSourcesOrTargets("sourcesId"))
router.delete("/actions/:actionNamespace/:actionName/sources/:datasetNamespace/:datasetName/:eventTypeName",
  swaggerValidator, namespacesController.requireNamespace("actionNamespace"), actionsController.requireAction,
  namespacesController.requireNamespace("datasetNamespace"), datasetsController.requireDataset,
  eventTypesController.requireEventType, usersController.authenticate(false), jobsController.checkKey(false),
  actionsController.removeSourceOrTarget("sourcesId"))
router.post("/actions/:actionNamespace/:actionName/sources/:datasetNamespace/:datasetName/:eventTypeName",
  swaggerValidator, namespacesController.requireNamespace("actionNamespace"), actionsController.requireAction,
  namespacesController.requireNamespace("datasetNamespace"), usersController.authenticate(false),
  jobsController.checkKey(false), actionsController.registerAndNotifySource)
router.get("/actions/:actionNamespace/:actionName/targets", swaggerValidator,
  namespacesController.requireNamespace("actionNamespace"), actionsController.requireAction,
  actionsController.listSourcesOrTargets("targetsId"))
router.post("/actions/:actionNamespace/:actionName/targets", swaggerValidator,
  // Caution: No bodyParser, because notification body has no predefined type.
  namespacesController.requireNamespace("actionNamespace"), actionsController.requireAction,
  usersController.authenticate(false), jobsController.checkKey(false), actionsController.notifyTargets)
router.delete("/actions/:actionNamespace/:actionName/targets/:datasetNamespace/:datasetName/:eventTypeName",
  swaggerValidator, namespacesController.requireNamespace("actionNamespace"), actionsController.requireAction,
  namespacesController.requireNamespace("datasetNamespace"), datasetsController.requireDataset,
  eventTypesController.requireEventType, usersController.authenticate(false), jobsController.checkKey(false),
  actionsController.removeSourceOrTarget("targetsId"))
router.post("/actions/:actionNamespace/:actionName/targets/:datasetNamespace/:datasetName/:eventTypeName",
  swaggerValidator, // Caution: No bodyParser, because notification body has no predefined type.
  namespacesController.requireNamespace("actionNamespace"), actionsController.requireAction,
  namespacesController.requireNamespace("datasetNamespace"), usersController.authenticate(false),
  jobsController.checkKey(false), actionsController.registerAndNotifyTarget)

router.get("/datasets", swaggerValidator, datasetsController.list)
router.post("/datasets", bodyParser, swaggerValidator, usersController.authenticate(true), datasetsController.create)
// router.put("/datasets", bodyParser, swaggerValidator, datasetsController.update)
router.get("/datasets/:datasetNamespace", swaggerValidator, namespacesController.requireNamespace("datasetNamespace"),
  datasetsController.listNamespace)
router.delete("/datasets/:datasetNamespace/:datasetName", swaggerValidator,
  namespacesController.requireNamespace("datasetNamespace"), datasetsController.requireDataset,
  usersController.authenticate(true), datasetsController.del)
router.get("/datasets/:datasetNamespace/:datasetName", swaggerValidator,
  namespacesController.requireNamespace("datasetNamespace"), datasetsController.requireDataset,
  usersController.authenticate(false), datasetsController.get)
router.patch("/datasets/:datasetNamespace/:datasetName", bodyParser, swaggerValidator,
  namespacesController.requireNamespace("datasetNamespace"), datasetsController.requireDataset,
  usersController.authenticate(true), datasetsController.patch)
router.get("/datasets/:datasetNamespace/:datasetName/actions", swaggerValidator,
  namespacesController.requireNamespace("datasetNamespace"), datasetsController.requireDataset,
  datasetsController.listActions)
router.delete("/datasets/:datasetNamespace/:datasetName/events/:eventTypeName", swaggerValidator,
  namespacesController.requireNamespace("datasetNamespace"), datasetsController.requireDataset,
  eventTypesController.requireEventType, usersController.authenticate(true), eventTypesController.deleteEvents)
router.get("/datasets/:datasetNamespace/:datasetName/events/:eventTypeName", swaggerValidator,
  namespacesController.requireNamespace("datasetNamespace"), datasetsController.requireDataset,
  eventTypesController.requireEventType, eventTypesController.listEvents)

// router.get("/events", swaggerValidator, eventsController.list)
router.delete("/events/:datasetNamespace/:datasetName", swaggerValidator,
  namespacesController.requireNamespace("datasetNamespace"), datasetsController.requireDataset,
  usersController.authenticate(true), eventTypesController.deleteAll)
router.get("/events/:datasetNamespace/:datasetName", swaggerValidator,
  namespacesController.requireNamespace("datasetNamespace"), datasetsController.requireDataset,
  eventTypesController.list)
router.delete("/events/:datasetNamespace/:datasetName/:eventTypeName", swaggerValidator,
  namespacesController.requireNamespace("datasetNamespace"), datasetsController.requireDataset,
  eventTypesController.requireEventType, usersController.authenticate(true), eventTypesController.del)
router.get("/events/:datasetNamespace/:datasetName/:eventTypeName", swaggerValidator,
  namespacesController.requireNamespace("datasetNamespace"), datasetsController.requireDataset,
  eventTypesController.requireEventType, eventTypesController.get)
router.post("/events/:datasetNamespace/:datasetName/:eventTypeName", swaggerValidator,  // Caution: No bodyParser
  namespacesController.requireNamespace("datasetNamespace"), datasetsController.requireDataset,
  datasetsController.checkEventKey(true), eventTypesController.notify)
router.delete("/events/:datasetNamespace/:datasetName/:eventTypeName/:createdAt", swaggerValidator,
  namespacesController.requireNamespace("datasetNamespace"), datasetsController.requireDataset,
  eventTypesController.requireEventType, eventsController.requireEvent, usersController.authenticate(true),
  eventsController.del)
router.get("/events/:datasetNamespace/:datasetName/:eventTypeName/:createdAt", swaggerValidator,
  namespacesController.requireNamespace("datasetNamespace"), datasetsController.requireDataset,
  eventTypesController.requireEventType, eventsController.requireEvent, eventsController.get)
router.post("/events/:datasetNamespace/:datasetName/:eventTypeName/:createdAt/redo", swaggerValidator,
  namespacesController.requireNamespace("datasetNamespace"), datasetsController.requireDataset,
  eventTypesController.requireEventType, eventsController.requireEvent, usersController.authenticate(true),
  eventsController.redo)

// router.get("/jobs", swaggerValidator, namespacesController.requireNamespace("actionNamespace"), jobsController.list)
router.delete("/jobs/:actionNamespace/:actionName", swaggerValidator,
  namespacesController.requireNamespace("actionNamespace"), actionsController.requireAction,
  usersController.authenticate(true), actionsController.deleteJobs)
router.get("/jobs/:actionNamespace/:actionName", swaggerValidator,
  namespacesController.requireNamespace("actionNamespace"), actionsController.requireAction, actionsController.listJobs)
router.post("/jobs/:actionNamespace/:actionName", swaggerValidator,
  // Caution: No bodyParser, because notification body has no predefined type.
  namespacesController.requireNamespace("actionNamespace"), actionsController.requireAction,
  usersController.authenticate(false), actionsController.checkKey(false), jobsController.create)
router.delete("/jobs/:actionNamespace/:actionName/:createdAt", swaggerValidator,
  namespacesController.requireNamespace("actionNamespace"), actionsController.requireAction, jobsController.requireJob,
  usersController.authenticate(true), jobsController.del)
router.get("/jobs/:actionNamespace/:actionName/latest", swaggerValidator,
  namespacesController.requireNamespace("actionNamespace"), actionsController.requireAction,
  jobsController.requireLatestJob, jobsController.get)
router.get("/jobs/:actionNamespace/:actionName/:createdAt", swaggerValidator,
  namespacesController.requireNamespace("actionNamespace"), actionsController.requireAction, jobsController.requireJob,
  jobsController.get)
router.post("/jobs/:actionNamespace/:actionName/latest/redo", swaggerValidator,
  namespacesController.requireNamespace("actionNamespace"), actionsController.requireAction,
  jobsController.requireLatestJob, usersController.authenticate(true), jobsController.redo)
router.post("/jobs/:actionNamespace/:actionName/:createdAt/redo", swaggerValidator,
  namespacesController.requireNamespace("actionNamespace"), actionsController.requireAction, jobsController.requireJob,
  usersController.authenticate(true), jobsController.redo)

router.post("/login", bodyParser, swaggerValidator, usersController.login)

router.get("/swagger.json", swaggerController.get)

router.get("/organizations", swaggerValidator, organizationsController.listUrlNames)
router.post("/organizations", bodyParser, swaggerValidator, usersController.authenticate(true),
  organizationsController.create)
router.delete("/organizations/:organizationName", swaggerValidator, organizationsController.requireOrganization,
  usersController.authenticate(true), organizationsController.del)
router.get("/organizations/:organizationName", swaggerValidator, organizationsController.requireOrganization,
  usersController.authenticate(false), organizationsController.get)

router.get("/users", swaggerValidator, usersController.listUrlNames)
router.post("/users", bodyParser, swaggerValidator, usersController.create)
// router.put("/users", bodyParser, swaggerValidator, usersController.update)
router.delete("/users/:userName", swaggerValidator, usersController.requireUser, usersController.authenticate(true),
  usersController.del)
router.get("/users/:userName", swaggerValidator, usersController.requireUser, usersController.authenticate(false),
  usersController.get)
// router.patch("/users/:userName", swaggerValidator, usersController.requireUser, usersController.patch)

let app = new Koa()
app.keys = config.keys
app.name = config.title
app.proxy = config.proxy
app
  .use(async function (ctx, next) {
    // Error handling middleware
    try {
      await next()
    } catch (e) {
      ctx.status = e.status || 500
      ctx.body = {
        apiVersion: "1",
        code: 500,
        message: e.message || http.STATUS_CODES[ctx.status],
      }
      ctx.app.emit("error", e, ctx)
    }
  })
  .use(convert(corsFactory()))
  .use(router.routes())
  .use(router.allowedMethods())


function startKoa() {
  let host = config.listen.host
  let port = config.listen.port || config.port
  app.listen(port, host)
  console.log(`Listening on ${host || "*"}:${port}...`)
}
